#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source $DIR/environment 2>/dev/null
if [ $? != "0" ] ; then
	echo -e "Cannot import environment path" >&2
	exit 1
fi



##########################
# Checking configuration #
##########################

echo -e "Checking configuration...\n" >&2

echo -n "Checking python2.7... "
if [ ! `command -v python2.7 2>/dev/null` ] ; then
	echo -e "FAILED!\n\nPlease install python2.7 and retry."
	exit 1
fi
echo "OK!"

echo -n "Checking virtualenv... "
if [ ! `command -v virtualenv 2>/dev/null` ] ; then
	echo -e "FAILED!\n\nPlease install virtualenv and retry."
	exit 1
fi
echo "OK!"

echo -n "Checking pip... "
if [ ! `command -v pip 2>/dev/null` ] ; then
	echo -e "FAILED!\n\nPlease install pip and retry."
	exit 1
fi
echo "OK!"

echo -n "Checking environment path... "
if [[ $VIRT_ENV =~ ^.*[[:space:]].*$ ]] ; then
	echo -e "FAILED!\n\nENVIRONMENT path must not contain spaces" >&2
	exit 1
fi
echo "OK!"



echo -ne "\nInstalling virtual environment... "

if [ `virtualenv $VIRT_ENV >/dev/null` ] ; then
	echo -e "FAILED!\n\nCannot install virtual environment.\nIs the directory writable?" >&2
	exit 1
fi
echo "OK!"



source $VIRT_ENV/bin/activate
if [ $? != "0" ] ; then
	echo -e "Cannot access virtual environment" >&2
	exit 1
fi



#######################
# Installing packages #
#######################

# Requirements for Django
echo -ne "Installing Django... "
pip install Django >/dev/null
if [ $? != "0" ] ; then
	echo -e "FAILED!\n\nCannot install Django." >&2
	exit 1
fi
echo "OK!"

# Requirements for Django REST framework
echo -ne "Installing Django REST framework."
pip install djangorestframework >/dev/null
if [ $? != "0" ] ; then
	echo -e "FAILED!\n\nCannot install Django REST framework." >&2
	exit 1
fi
echo -n "."
pip install markdown >/dev/null       # Markdown support for the browsable API.
if [ $? != "0" ] ; then
	echo -e "FAILED!\n\nCannot install Django REST framework." >&2
	exit 1
fi
echo -n ". "
pip install django-filter >/dev/null  # Filtering support
if [ $? != "0" ] ; then
	echo -e "FAILED!\n\nCannot install Django REST framework." >&2
	exit 1
fi
echo "OK!"

# Requirements for Java calls from python
echo -ne "Installing Py4j... "
easy_install py4j > /dev/null
if [ $? != "0" ] ; then
	echo -e "FAILED!\n\nCannot install Py4j." >&2
	exit 1
fi
echo "OK!"



#####################
# Setting up Django #
#####################

echo -ne "Copying py4j library for \"java-security-api\" Eclipse project... "
if [ -e $VIRT_ENV/lib/python2.7/site-packages/py4j-0.8.2.1-py2.7.egg/share/py4j/py4j0.8.2.1.jar ] ; then
	cp $VIRT_ENV/lib/python2.7/site-packages/py4j-0.8.2.1-py2.7.egg/share/py4j/py4j0.8.2.1.jar $GATEWAY_SERV_PATH/py4j0.8.2.1.jar
	echo "OK!"
else
	echo -e "FAILED!\n\nCannot find py4j library, check manually." >&2
	exit 1
fi



#####################
# Setting up Django #
#####################

echo -e "\nSetting up Django..."
python $DJANGO_PROJ/manage.py migrate > /dev/null
if [ $? != "0" ] ; then
	echo -e "Error synchronizing database. Check manually." >&2
	exit 1
fi

read -p "Do you want to create a new superuser [Y|n]? " -n 1 -r
echo
if [[ $REPLY =~ ^[Nn]$ ]]
then
	echo "You can always create a superuser using script create-superuser.sh"
else
	echo "Creating superuser..."
	python $DJANGO_PROJ/manage.py createsuperuser
	if [ $? != "0" ] ; then
		echo -e "Error creating superuser" >&2
		exit 1
	fi
fi

echo -e "\nEND"

exit 0
