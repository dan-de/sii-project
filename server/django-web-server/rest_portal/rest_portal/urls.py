from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',

	# REST web authentication
	url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
	
	# Django administration site
    url(r'^admin/', include(admin.site.urls)),
    
    # User management and main web application
    url(r'^', include('users.urls', namespace='users')),
    
    # Documents management
    url(r'^rest/docs/', include('documents.urls', namespace='documents')),
)
