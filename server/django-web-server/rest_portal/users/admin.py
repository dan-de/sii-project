from django.contrib import admin
from users import models

# Register your models here.
class CredentialAdmin(admin.ModelAdmin):
	fieldsets = [
		('User',			{'fields': ['user']}),
		('RSA Key',			{'fields': ['pubKey', 'wrappedPrivKey', 'iv', 'salt'], 'classes': ['collapse']}),
		('DSA Key',			{'fields': ['pubDSAKey', 'wrappedPrivDSAKey', 'ivDSA', 'saltDSA'], 'classes': ['collapse']}),
	]
	
	readonly_fields = ['user', 'pubKey', 'wrappedPrivKey', 'iv', 'salt', 'pubDSAKey', 'wrappedPrivDSAKey', 'ivDSA', 'saltDSA']
	
	list_display = ['pk', 'user']
	search_fields = ['pk', 'user']
	ordering = ['user']

admin.site.register(models.Credential, CredentialAdmin)



class LevelAdmin(admin.ModelAdmin):
	fieldsets = [
		('',				{'fields': ['levNumber']}),
	]
	
	readonly_fields = ['levNumber',]
	
	list_display = ['pk', 'levNumber',]
	search_fields = ['pk', 'levNumber',]
	ordering = ['levNumber',]

admin.site.register(models.Level, LevelAdmin)



class UserLevelAdmin(admin.ModelAdmin):
	fieldsets = [
		('',				{'fields': ['user', 'level', 'cryptedAESKey']}),
	]
	
	readonly_fields = ['user', 'level', 'cryptedAESKey']
	
	list_display = ['pk', 'user', 'level']
	search_fields = ['pk', 'user', 'level']
	ordering = ['user', 'level']

admin.site.register(models.UserLevel, UserLevelAdmin)
