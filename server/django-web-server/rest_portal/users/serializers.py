from rest_framework import serializers
from users import models

class CredentialSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.SerializedCredential
		fields = ('username', 'pubKeyBase64', 'wrappedPrivKeyBase64', 'ivBase64', 'saltBase64', 'pubDSAKeyBase64', 'wrappedPrivDSAKeyBase64', 'ivDSABase64', 'saltDSABase64')

class LevelListSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.Level
		fields = ('levNumber',)

class LevelSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.SerializedUserLevel
		fields = ('username', 'level', 'cryptedAESKeyBase64')

class UserDSAKeySerializer(serializers.ModelSerializer):
	class Meta:
		model = models.SerializedUserPublicDSAKey
		fields = ('username', 'pubDSAKeyBase64')
