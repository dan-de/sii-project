from django.db import models
from django.conf import settings

# Create your models here.
class Credential(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, unique=True)
	
	pubKey = models.FileField(upload_to='users/pubkeys')
	wrappedPrivKey = models.FileField(upload_to='users/wrappedprivkeys')
	iv = models.FileField(upload_to='users/ivs')
	salt = models.FileField(upload_to='users/salt')
	
	pubDSAKey = models.FileField(upload_to='users/pubDSAkeys')
	wrappedPrivDSAKey = models.FileField(upload_to='users/wrappedprivDSAkeys')
	ivDSA = models.FileField(upload_to='users/ivsDSA')
	saltDSA = models.FileField(upload_to='users/saltDSA')
	
	def __unicode__(self):  # Python 3: def __str__(self):
		return str(self.user)

class Level(models.Model):
	levNumber = models.IntegerField(unique=True)
	
	def __unicode__(self):  # Python 3: def __str__(self):
		return str(self.levNumber)

class UserLevel(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	level = models.ForeignKey(Level, on_delete=models.CASCADE)
	cryptedAESKey = models.FileField(upload_to='levels/cryptedaeskeys')
	
	def __unicode__(self):  # Python 3: def __str__(self):
		return str('Level ' + str(self.level) + ' for user ' + str(self.user))
	
	class Meta:
		unique_together = ('user', 'level')



# Serialization classes
# They should be transient class models
class SerializedCredential(models.Model):
	username = models.CharField(max_length=30)
	
	pubKeyBase64 = models.CharField(max_length=4096)
	wrappedPrivKeyBase64 = models.CharField(max_length=4096)
	ivBase64 = models.CharField(max_length=4096)
	saltBase64 = models.CharField(max_length=4096)
	
	pubDSAKeyBase64 = models.CharField(max_length=4096)
	wrappedPrivDSAKeyBase64 = models.CharField(max_length=4096)
	ivDSABase64 = models.CharField(max_length=4096)
	saltDSABase64 = models.CharField(max_length=4096)

class SerializedUserLevel(models.Model):
	username = models.CharField(max_length=30)
	level = models.IntegerField()
	
	cryptedAESKeyBase64 = models.CharField(max_length=4096)

class SerializedUserPublicDSAKey(models.Model):
	username = models.CharField(max_length=30)
	pubDSAKeyBase64 = models.CharField(max_length=4096)
