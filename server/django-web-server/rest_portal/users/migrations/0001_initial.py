# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Credential',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pubKey', models.FileField(upload_to=b'users/pubkeys')),
                ('wrappedPrivKey', models.FileField(upload_to=b'users/wrappedprivkeys')),
                ('iv', models.FileField(upload_to=b'users/ivs')),
                ('salt', models.FileField(upload_to=b'users/salt')),
                ('pubDSAKey', models.FileField(upload_to=b'users/pubDSAkeys')),
                ('wrappedPrivDSAKey', models.FileField(upload_to=b'users/wrappedprivDSAkeys')),
                ('ivDSA', models.FileField(upload_to=b'users/ivsDSA')),
                ('saltDSA', models.FileField(upload_to=b'users/saltDSA')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Level',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('levNumber', models.IntegerField(unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SerializedCredential',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=30)),
                ('pubKeyBase64', models.CharField(max_length=4096)),
                ('wrappedPrivKeyBase64', models.CharField(max_length=4096)),
                ('ivBase64', models.CharField(max_length=4096)),
                ('saltBase64', models.CharField(max_length=4096)),
                ('pubDSAKeyBase64', models.CharField(max_length=4096)),
                ('wrappedPrivDSAKeyBase64', models.CharField(max_length=4096)),
                ('ivDSABase64', models.CharField(max_length=4096)),
                ('saltDSABase64', models.CharField(max_length=4096)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SerializedUserLevel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=30)),
                ('level', models.IntegerField()),
                ('cryptedAESKeyBase64', models.CharField(max_length=4096)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SerializedUserPublicDSAKey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=30)),
                ('pubDSAKeyBase64', models.CharField(max_length=4096)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserLevel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cryptedAESKey', models.FileField(upload_to=b'levels/cryptedaeskeys')),
                ('level', models.ForeignKey(to='users.Level')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='userlevel',
            unique_together=set([('user', 'level')]),
        ),
    ]
