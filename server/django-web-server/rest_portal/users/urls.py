from django.conf.urls import patterns, url, include

from users import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    
    # Account management
    url(r'^accounts/login/$', views.login, name='login'),
    url(r'^accounts/logout/$', views.logout, name='logout'),
    
    # System wide operations
    url(r'^init/$', views.initSystem, name='init'),
    url(r'^levels/$', views.manageLevels, name='levels'),
    
    # User management
    url(r'^users/create/$', views.createUser, name='createuser'),
    url(r'^users/delete/$', views.deleteUser, name='deleteuser'),
    url(r'^users/perm/$', views.manageUserPermissions, name='userpermissions'),
    
    # REST interface for user management
    url(r'^rest/credential/$', views.credentialREST.as_view(), name='restcredential'),
    url(r'^rest/users/(?P<username>[a-zA-Z@.+-_]*)/$', views.userDSAPublicKeyREST.as_view(), name='restuserdsakey'),
    url(r'^rest/levels/$', views.listUserLevelsREST.as_view(), name='restuserlevellist'),
    url(r'^rest/levels/(?P<level>[0-9]*)/$', views.userLevelREST.as_view(), name='restuserlevel'),
)
