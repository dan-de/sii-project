from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.conf import settings

import django.contrib.auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model

from django.core.files.base import ContentFile

from py4j.java_gateway import JavaGateway, Py4JNetworkError

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions

import base64

from users import models
from users import serializers



"""
Can't understand if Django closes files automatically or not.
In case of 'IOError: [Errno 24] Too many open files' error
try closing every FileField opened with read() method.
For example:
pubKey = credential.pubKey.read()
credential.pubKey.file.close()
"""



# These are not views!
def getLevelsID(user):
	"""
	Query explaination:
	From Level objects set, get the subset of objects which are ForeignKey in UserLevel objects set that has user as User field.
	Then from this Level objects subset extract levNumber field and construct an ordered list of them
	"""
	return models.Level.objects.filter(userlevel__user=user).values_list('levNumber', flat=True).order_by('levNumber')

def getMaxLevelID(user):
	levels = getLevelsID(user)
	return max(levels)

def getMinUpdateLevel(currentUser, targetUser):
	if currentUser.is_superuser:
		return 0
	else:
		return getMaxLevelID(targetUser)

def getOtherUsersList(user):
	"""
	Query explaination:
	From User objects set, get the subset of objects which have is_superuser attribute False and exclude the object which username is user.username.
	Then from this User objects subset extract username field and construct an ordered list of them
	"""
	return get_user_model().objects.filter(is_superuser=False).exclude(username=user.username).values_list('username', flat=True).order_by('username')

def delUserObjects(userobj):
	credentialobj = models.Credential.objects.get(user=userobj)
	userlevelobjs = models.UserLevel.objects.filter(user=userobj)
	
	# Remove all files
	for u in userlevelobjs:
		u.cryptedAESKey.delete(save=True)
	credentialobj.pubKey.delete(save=False)
	credentialobj.wrappedPrivKey.delete(save=False)
	credentialobj.iv.delete(save=False)
	credentialobj.salt.delete(save=False)
	credentialobj.pubDSAKey.delete(save=False)
	credentialobj.wrappedPrivDSAKey.delete(save=False)
	credentialobj.ivDSA.delete(save=False)
	credentialobj.saltDSA.delete(save=False)
	credentialobj.save()
	
	# Remove objects
	userobj.delete()
	# CASCADE will do the rest ;)



# Views

def index(request):
	initialized = True if models.Credential.objects.all() else False
	content = {
				'user': request.user,
				'initialized': initialized
	}
	return render(request, 'users/home.html', content)



@login_required
def initSystem(request):
	if request.method == 'GET':
		
		if models.Credential.objects.all() or not request.user.is_superuser:
			return HttpResponseRedirect(reverse('users:index'))
		
		if request.user.is_superuser:
			return render(request, 'users/init_system.html', {'user': request.user})
		
	elif request.method == 'POST' and request.user.is_superuser and not models.Credential.objects.all():
		
		# Retrieve components for personal keyring
		username = request.user.username
		password = request.POST['password']
		salt = settings.KEYRING_SALT
		
		# Check if password is correct
		user = django.contrib.auth.authenticate(username=username, password=password)
		if user is None:
			return render(request, 'users/init_system.html', {'error_message': 'Invalid password'})
		
		# Get Java interface
		# Remember to start JVM server first!!!
		gateway = JavaGateway()
		
		javaObject = gateway.entry_point.getObject()
		
		# Get keys element
		userpubkey = javaObject.getPublicKey()
		#userprivkey = javaObject.getPrivateKey()
		#wraparray = javaObject.wrapPrivateRSAKey(userprivkey, username, password, salt)
		wraparray = javaObject.getWrappedRSAPrivKey(username, password, salt)
		userivwrappedkey = wraparray[0]
		userwrappedkey = wraparray[1]
		
		userpubdsakey = javaObject.getDSAPublicKey()
		#userprivdsakey = javaObject.getDSAPrivateKey()
		#wraparraydsa = javaObject.wrapPrivateDSAKey(userprivdsakey, username, password, salt)
		wraparraydsa = javaObject.getWrappedDSAPrivKey(username, password, salt)
		userivwrappedsakey = wraparraydsa[0]
		userwrappeddsakey = wraparraydsa[1]
		
		# Save keyring
		credential = models.Credential()
		
		credential.user = user
		
		credential.pubKey.save(user.username, ContentFile(userpubkey), save=False)
		credential.wrappedPrivKey.save(user.username, ContentFile(userwrappedkey), save=False)
		credential.iv.save(user.username, ContentFile(userivwrappedkey), save=False)
		credential.salt.save(user.username, ContentFile(salt), save=False)
		
		credential.pubDSAKey.save(user.username, ContentFile(userpubdsakey), save=False)
		credential.wrappedPrivDSAKey.save(user.username, ContentFile(userwrappeddsakey), save=False)
		credential.ivDSA.save(user.username, ContentFile(userivwrappedsakey), save=False)
		credential.saltDSA.save(user.username, ContentFile(salt), save=False)
		
		credential.save()
		
		# Create authorization levels
		for i in range(0,4):
			
			javaObject.renewAES()
			levAESKey = javaObject.getAESKey()
			levEncryptedAESKey = javaObject.wrapAESWithRSA(levAESKey, userpubkey)
			
			lev = models.Level()
			
			lev.levNumber = i
			lev.save()
			# Refetch object from DB now that it was saved (dunno if necessary)
			lev = models.Level.objects.get(levNumber=i)
			
			userLev = models.UserLevel()
			userLev.user = user
			userLev.level = lev
			userLev.cryptedAESKey.save(user.username + '_' + str(i), ContentFile(levEncryptedAESKey), save=False)
			
			userLev.save()
			
	return HttpResponseRedirect(reverse('users:index'))



@login_required
def manageLevels(request):
	# Return on home page if system was not initialized
	if not models.Credential.objects.all():
		return HttpResponseRedirect(reverse('users:index'))
	
	if request.method == 'GET' and request.user.is_superuser:
		
		maxLevel = getMaxLevelID(request.user)
		content = {
					'user': request.user,
					'maxlevel': maxLevel,
					'page': 'home'
		}
		return render(request, 'users/manage_levels.html', content)
		
	elif request.method == 'POST' and request.user.is_superuser:
		if request.POST['action'] == 'next':
			
			### Render adding page ###
			
			newLevel = request.POST['level']
			password = request.POST['password']
			
			if newLevel == '':
				maxLevel = getMaxLevelID(request.user)
				content = {
							'user': request.user,
							'page': 'home',
							'maxlevel': maxLevel,
							'error_message': 'Invalid level number'
				}
				return render(request, 'users/manage_levels.html', content)
			
			if not int(newLevel) in range(0,101):
				maxLevel = getMaxLevelID(request.user)
				content = {
							'user': request.user,
							'maxlevel': maxLevel,
							'page': 'home',
							'error_message': 'You must type a value between 0 and 100'
				}
				return render(request, 'users/manage_levels.html', content)
			
			content = {
						'user': request.user,
						'page': 'confirmation',
						'password': password,
						'newlevel': newLevel
			}
			return render(request, 'users/manage_levels.html', content)
		
		elif request.POST['action'] == 'confirm':
			
			### Add new level (if possible) ###
			
			# Fetch required elements from POST
			newLevelNumber = request.POST['newlevel']
			if newLevelNumber == '':
				maxLevel = getMaxLevelID(request.user)
				content = {
							'user': request.user,
							'page': 'home',
							'maxlevel': maxLevel,
							'error_message': 'Invalid level number'
				}
				return render(request, 'users/manage_levels.html', content)
			
			if not int(newLevelNumber) in range(0,101):
				maxLevel = getMaxLevelID(request.user)
				content = {
							'user': request.user,
							'maxlevel': maxLevel,
							'page': 'home',
							'error_message': 'You must type a value between 0 and 100'
				}
				return render(request, 'users/manage_levels.html', content)
			
			newLevelNumber = int(newLevelNumber)
			
			username = request.user.username
			password = request.POST['password']
			
			# Check password
			user = django.contrib.auth.authenticate(username=username, password=password)
			if user is None:
				maxLevel = getMaxLevelID(request.user)
				content = {
							'user': request.user,
							'page': 'home',
							'maxlevel': maxLevel,
							'error_message': 'Invalid password'
				}
				return render(request, 'users/manage_levels.html', content)
			
			# Check if level is equal
			currentLevel = getMaxLevelID(request.user)
			if currentLevel == newLevelNumber:
				maxLevel = getMaxLevelID(request.user)
				content = {
							'user': request.user,
							'page': 'home',
							'maxlevel': maxLevel,
							'error_message': 'You have chosen the same level'
				}
				return render(request, 'users/manage_levels.html', content)
			
			if newLevelNumber > currentLevel:
				
				### Add all levels from currentLevel+1 to newLevelNumber ###
				
				# Get user credentials
				credential = models.Credential.objects.get(user=user)
				
				# Get Java interface
				# Remember to start JVM server first!!!
				gateway = JavaGateway()
				javaObject = gateway.entry_point.getObject()
				
				# Get keys
				pubKey = credential.pubKey.read()
				privKey = credential.wrappedPrivKey.read()
				iv = credential.iv.read()
				salt = credential.salt.read()
				
				# Need to use Java arrays objects in order to call java method
				pubKeyArray = gateway.new_array(gateway.jvm.byte, len(pubKey))
				for i in range(0,len(pubKey)):
					pubKeyArray[i] = pubKey[i]
				
				privKeyArray = gateway.new_array(gateway.jvm.byte, len(privKey))
				for i in range(0,len(privKey)):
					privKeyArray[i] = privKey[i]
				
				ivArray = gateway.new_array(gateway.jvm.byte, len(iv))
				for i in range(0,len(iv)):
					ivArray[i] = iv[i]
				
				# Unwrap user private RSA key in order to encrypt new level key
				unwrappedPrivKey = javaObject.unwrapPivateRSAKey(privKeyArray, ivArray, username, password, str(salt))
				
				for l in range(currentLevel+1, newLevelNumber+1):
				
					# Generate and wrap new level key
					javaObject.renewAES()
					levAESKey = javaObject.getAESKey()
					levEncryptedAESKey = javaObject.wrapAESWithRSA(levAESKey, pubKeyArray)
					
					# Add this level in the system
					lev = models.Level()
					
					lev.levNumber = l
					lev.save()
					# Refetch object from DB now that it was saved (dunno if necessary)
					lev = models.Level.objects.get(levNumber=l)
					
					# Add the key to the associated level
					userLev = models.UserLevel()
					userLev.user = user
					userLev.level = lev
					userLev.cryptedAESKey.save(user.username + '_' + str(l), ContentFile(levEncryptedAESKey), save=False)
					
					userLev.save()
			
			else:
				
				### Remove all levels from newLevelNumber+1 to currentLevel ###
				
				for l in range(newLevelNumber+1, currentLevel+1):
					levelobj = models.Level.objects.get(levNumber=l)
					userslevel = models.UserLevel.objects.filter(level=levelobj)
					
					for ul in userslevel:
						if ul.cryptedAESKey:
							ul.cryptedAESKey.delete(save=True)
					
					levelobj.delete()
				
			# Return to previous page with a confirmation message
			maxLevel = getMaxLevelID(request.user)
			content = {
						'user': request.user,
						'maxlevel': maxLevel,
						'page': 'home',
						'confirm_message': 'Operation completed'
			}
			return render(request, 'users/manage_levels.html', content)
		
	return HttpResponseRedirect(reverse('users:index'))



@login_required
def createUser(request):
	# Return on home page if system was not initialized
	if not models.Credential.objects.all():
		return HttpResponseRedirect(reverse('users:index'))
	
	if request.method == 'GET':
		levels = getLevelsID(request.user)
		return render(request, 'users/create_user.html', {'user': request.user, 'levels': levels})
	
	if request.method == 'POST':
		# Get current user credential
		username = request.user.username
		password = request.POST['password']
		
		# Check password
		user = django.contrib.auth.authenticate(username=username, password=password)
		if user is None:
			levels = getLevelsID(request.user)
			content = {
						'user': request.user,
						'levels': levels,
						'error_message': 'Invalid password'
			}
			return render(request, 'users/create_user.html', content)
		
		# Get new user credentials from POST
		newusername = request.POST['newusername']
		newpassword1 = request.POST['newpassword1']
		newpassword2 = request.POST['newpassword2']
		newemail = request.POST['newemail']
		newfirstname = request.POST['newfirstname']
		newlastname = request.POST['newlastname']
		newlevel = int(request.POST['newuserlevel'])
		
		# Check if passwords fields match
		if newpassword1 != newpassword2:
			levels = getLevelsID(request.user)
			content = {
						'user': request.user,
						'levels': levels,
						'error_message': 'Passwords mismatch'
			}
			return render(request, 'users/create_user.html', content)
		
		# Check if user already exists
		newuser = get_user_model().objects.filter(username=newusername)
		if newuser:
			levels = getLevelsID(request.user)
			content = {
						'user': request.user,
						'levels': levels,
						'error_message': 'This user already exists'
			}
			return render(request, 'users/create_user.html', content)
		
		### Start user creation ###
		
		# Create new User in the system
		newuser = get_user_model().objects.create_user(newusername, newemail, newpassword1)
		newuser.first_name = newfirstname
		newuser.last_name = newlastname
		newuser.is_staff = False
		newuser.is_superuser = False
		newuser.save()
		# Refetch object from DB now that it was saved (dunno if necessary)
		newuser = django.contrib.auth.authenticate(username=newusername, password=newpassword1)
		
		# Get Java interface
		# Remember to start JVM server first!!!
		gateway = JavaGateway()
		javaObject = gateway.entry_point.getObject()
		
		### Fetch current user keys ###
		
		# Get user credentials
		credential = models.Credential.objects.get(user=user)
		
		# Get keys
		pubKey = credential.pubKey.read()
		privKey = credential.wrappedPrivKey.read()
		iv = credential.iv.read()
		salt = credential.salt.read()
		
		# Need to use Java arrays objects in order to call java method
		pubKeyArray = gateway.new_array(gateway.jvm.byte, len(pubKey))
		for i in range(0,len(pubKey)):
			pubKeyArray[i] = pubKey[i]
		
		privKeyArray = gateway.new_array(gateway.jvm.byte, len(privKey))
		for i in range(0,len(privKey)):
			privKeyArray[i] = privKey[i]
		
		ivArray = gateway.new_array(gateway.jvm.byte, len(iv))
		for i in range(0,len(iv)):
			ivArray[i] = iv[i]
		
		# Unwrap user private RSA key in order to encrypt new level key
		unwrappedPrivKey = javaObject.unwrapPivateRSAKey(privKeyArray, ivArray, username, password, str(salt))
		
		unwrappedPrivKeyArray = gateway.new_array(gateway.jvm.byte, len(unwrappedPrivKey))
		for i in range(0,len(unwrappedPrivKey)):
			unwrappedPrivKeyArray[i] = unwrappedPrivKey[i]
		
		### Create new user keys ###
		
		# Get new keys element
		userpubkey = javaObject.getPublicKey()
		userprivkey = javaObject.getPrivateKey()
		wraparray = javaObject.wrapPrivateRSAKey(userprivkey, newusername, newpassword1, salt)
		userivwrappedkey = wraparray[0]
		userwrappedkey = wraparray[1]
		salt = settings.KEYRING_SALT
		
		userpubdsakey = javaObject.getDSAPublicKey()
		userprivdsakey = javaObject.getDSAPrivateKey()
		wraparraydsa = javaObject.wrapPrivateDSAKey(userprivdsakey, newusername, newpassword1, salt)
		userivwrappedsakey = wraparraydsa[0]
		userwrappeddsakey = wraparraydsa[1]
		
		# Save keyring
		credential = models.Credential()
		
		credential.user = newuser
		credential.pubKey.save(newuser.username, ContentFile(userpubkey), save=False)
		credential.wrappedPrivKey.save(newuser.username, ContentFile(userwrappedkey), save=False)
		credential.iv.save(newuser.username, ContentFile(userivwrappedkey), save=False)
		credential.salt.save(newuser.username, ContentFile(salt), save=False)
		credential.pubDSAKey.save(newuser.username, ContentFile(userpubdsakey), save=False)
		credential.wrappedPrivDSAKey.save(newuser.username, ContentFile(userwrappeddsakey), save=False)
		credential.ivDSA.save(newuser.username, ContentFile(userivwrappedsakey), save=False)
		credential.saltDSA.save(newuser.username, ContentFile(salt), save=False)
		
		credential.save()
		
		### Create authorization levels ###
		
		for l in range(0, newlevel+1):
			ulk =  models.UserLevel.objects.get(user=user, level__levNumber=l)
			
			levWrappedAESKey = ulk.cryptedAESKey.read()
			
			levWrappedAESKeyArray = gateway.new_array(gateway.jvm.byte, len(levWrappedAESKey))
			for i in range(0,len(levWrappedAESKey)):
				levWrappedAESKeyArray[i] = levWrappedAESKey[i]
			
			# Get the AES key for current level
			levAESKey = javaObject.unwrapAESWithRSA(levWrappedAESKeyArray, unwrappedPrivKeyArray)
			
			levAESKeyArray = gateway.new_array(gateway.jvm.byte, len(levAESKey))
			for i in range(0,len(levAESKey)):
				levAESKeyArray[i] = levAESKey[i]
			
			# Wrap AES level key for new user
			newLevEncryptedAESKey = javaObject.wrapAESWithRSA(levAESKeyArray, userpubkey)
			
			newUserLev = models.UserLevel()
			
			newUserLev.user = newuser
			newUserLev.level = ulk.level
			newUserLev.cryptedAESKey.save(newuser.username + '_' + str(ulk.level.levNumber), ContentFile(newLevEncryptedAESKey), save=False)
			
			newUserLev.save()
		
	return HttpResponseRedirect(reverse('users:index'))



@login_required
def deleteUser(request):
	# Return on home page if system was not initialized
	if not models.Credential.objects.all():
		return HttpResponseRedirect(reverse('users:index'))
	
	if request.method == 'GET':
		content = {
					'user': request.user,
					'userlist': getOtherUsersList(request.user),
					'page': 'home'
		}
		return render(request, 'users/delete_user.html', content)
	
	if request.method == 'POST':
		if request.POST['action'] == 'currentuser':
			
			# Render confirm deletion page for current user
			return render(request, 'users/delete_user.html', {'user': request.user, 'page': 'confirmcurrent'})
		
		if request.POST['action'] == 'otheruser':
			
			# Render confirm deletion page for other user or error message if invalid user
			username = request.POST.get('username', '')
			if username == '':
				content = {
							'user': request.user,
							'userlist': getOtherUsersList(request.user),
							'page': 'home',
							'message2': 'Invalid user'
				}
				return render(request, 'users/delete_user.html', content)
			
			content = {
						'user': request.user,
						'deluser': username,
						'page': 'confirmother'
			}
			return render(request, 'users/delete_user.html', content)
			
		if request.POST['action'] == 'confirmdeletecurrent':
			
			### Delete current user ###
			
			# Return error if trying to delete the superuser
			if request.user.is_superuser:
				content = {
							'user': request.user,
							'userlist': getOtherUsersList(request.user),
							'page': 'home',
							'message2': 'Operation forbidden'
				}
				return render(request, 'users/delete_user.html', content)
			
			# Fetch user related object
			userobj = request.user
			
			# Logout current user
			django.contrib.auth.logout(request)
			
			# Delete all related files and objects in the database
			delUserObjects(userobj)
			
			return HttpResponseRedirect(reverse('users:index'))
			
		if request.POST['action'] == 'confirmdeleteother':
			
			### Delete other user ###
			
			# Return error if a non superuser user is trying to delete other user
			if not request.user.is_superuser:
				content = {
							'user': request.user,
							'userlist': getOtherUsersList(request.user),
							'page': 'home',
							'message2': 'Operation forbidden'
				}
				return render(request, 'users/delete_user.html', content)
			
			# Fetch user related objects
			username = request.POST['toremove']
			userobj = get_user_model().objects.get(username=username)
			
			if userobj.is_superuser:
				content = {
							'user': request.user,
							'userlist': getOtherUsersList(request.user),
							'page': 'home',
							'message2': 'Cannot remove a superuser'
				}
				return render(request, 'users/delete_user.html', content)
			
			# Should never happen: current user selected itself from the list
			if username == request.user.username:
				content = {
							'user': request.user,
							'userlist': getOtherUsersList(request.user),
							'page': 'home',
							'message2': 'Internal server error'
				}
				return render(request, 'users/delete_user.html', content)
			
			delUserObjects(userobj)
			
			content = {
						'user': request.user,
						'userlist': getOtherUsersList(request.user),
						'page': 'home',
						'message2': 'Done'
			}
			return render(request, 'users/delete_user.html', content)
			
		if request.POST['action'] == 'abortdeletecurrent':
			# Abort operation
			content = {
						'user': request.user,
						'userlist': getOtherUsersList(request.user),
						'page': 'home',
						'message1': 'Operation aborted'
			}
			return render(request, 'users/delete_user.html', content)
			
		if request.POST['action'] == 'abortdeleteother':
			# Abort operation
			content = {
						'user': request.user,
						'userlist': getOtherUsersList(request.user),
						'page': 'home',
						'message2': 'Operation aborted'
			}
			return render(request, 'users/delete_user.html', content)
	
	return HttpResponseRedirect(reverse('users:index'))



@login_required
def manageUserPermissions(request):
	# Return on home page if system was not initialized
	if not models.Credential.objects.all():
		return HttpResponseRedirect(reverse('users:index'))
	
	if request.method == 'GET':
		content = {
					'user': request.user,
					'userlist': getOtherUsersList(request.user),
					'page': '1'
		}
		return render(request, 'users/manage_user_permissions.html', content)
	
	elif request.method == 'POST':
		
		if request.POST.get('targetuser', '') == '':
			content = {
						'user': request.user,
						'userlist': getOtherUsersList(request.user),
						'page': '1',
						'error_message': 'You must select an user'
			}
			return render(request, 'users/manage_user_permissions.html', content)
		
		if request.POST['action'] == '1':
			
			### Page 1 ###
			
			targetUser = request.POST['targetuser']
			
			currentUserObj = request.user
			targetUserObj = get_user_model().objects.get(username=targetUser)
			
			maxLevelTargetUser = getMaxLevelID(targetUserObj)
			maxLevelCurrentUser = getMaxLevelID(currentUserObj)
			
			if maxLevelTargetUser > maxLevelCurrentUser:
				content = {
							'user': request.user,
							'userlist': getOtherUsersList(request.user),
							'page': '1',
							'error_message': 'You don\'t have enough right to update this user'
				}
				return render(request, 'users/manage_user_permissions.html', content)
			
			minLevel = getMinUpdateLevel(currentUserObj, targetUserObj)
			maxLevel = maxLevelCurrentUser
			
			content = {
						'user': request.user,
						'currentlevel': maxLevelTargetUser,
						'targetuser': targetUser,
						'minlevel': minLevel,
						'maxlevel': maxLevel,
						'page': '2'
			}
			return render(request, 'users/manage_user_permissions.html', content)
		
		elif request.POST['action'] == '2':
			
			### Page 2 ###
			
			targetUser = request.POST['targetuser']
			newLevelTargetUser = int(request.POST['level'])
			password = request.POST['password']
			
			currentUserObj = request.user
			targetUserObj = get_user_model().objects.get(username=targetUser)
			
			maxLevelTargetUser = int(getMaxLevelID(targetUserObj))
			maxLevelCurrentUser = int(getMaxLevelID(currentUserObj))
			
			minLevel = getMinUpdateLevel(currentUserObj, targetUserObj)
			maxLevel = maxLevelCurrentUser
			
			if newLevelTargetUser > maxLevelCurrentUser:
				content = {
							'user': request.user,
							'currentlevel': maxLevelTargetUser,
							'targetuser': targetUser,
							'minlevel': minLevel,
							'maxlevel': maxLevel,
							'page': '2',
							'error_message': 'You don\'t have enough right to update this user to this level'
				}
				return render(request, 'users/manage_user_permissions.html', content)
			
			if newLevelTargetUser == maxLevelTargetUser:
				content = {
							'user': request.user,
							'currentlevel': maxLevelTargetUser,
							'targetuser': targetUser,
							'minlevel': minLevel,
							'maxlevel': maxLevel,
							'page': '2',
							'error_message': 'You have chosen the same level as before'
				}
				return render(request, 'users/manage_user_permissions.html', content)
			
			if newLevelTargetUser < maxLevelTargetUser and not currentUserObj.is_superuser:
				content = {
							'user': request.user,
							'currentlevel': maxLevelTargetUser,
							'targetuser': targetUser,
							'minlevel': minLevel,
							'maxlevel': maxLevel,
							'page': '2',
							'error_message': 'You cannot remove security levels if you are not the administrator'
				}
				return render(request, 'users/manage_user_permissions.html', content)
			
			content = {
						'user': request.user,
						'password': password,
						'newlevel': newLevelTargetUser,
						'targetuser': targetUser,
						'page': '3'
			}
			return render(request, 'users/manage_user_permissions.html', content)
		
		
		
		elif request.POST['action'] == 'abort':
			content = {
						'user': request.user,
						'userlist': getOtherUsersList(request.user),
						'page': '1',
						'confirm_message': 'Operation aborted'
			}
			return render(request, 'users/manage_user_permissions.html', content)
		
		elif request.POST['action'] == 'confirm':
			
			targetUser = request.POST['targetuser']
			newLevelTargetUser = int(request.POST['newlevel'])
			password = request.POST['password']
			username = request.user.username
			
			targetUserObj = get_user_model().objects.get(username=targetUser)
			
			maxLevelTargetUser = int(getMaxLevelID(targetUserObj))
			maxLevelCurrentUser = int(getMaxLevelID(request.user))
			
			minLevel = getMinUpdateLevel(request.user, targetUserObj)
			maxLevel = maxLevelCurrentUser
			
			user = django.contrib.auth.authenticate(username=request.user.username, password=password)
			if user is None:
				content = {
							'user': request.user,
							'currentlevel': maxLevelTargetUser,
							'targetuser': targetUser,
							'minlevel': minLevel,
							'maxlevel': maxLevel,
							'page': '2',
							'error_message': 'Invalid password'
				}
				return render(request, 'users/manage_user_permissions.html', content)
			
			if newLevelTargetUser > maxLevelCurrentUser:
				content = {
							'user': request.user,
							'currentlevel': maxLevelTargetUser,
							'targetuser': targetUser,
							'minlevel': minLevel,
							'maxlevel': maxLevel,
							'page': '2',
							'error_message': 'You don\'t have enough right to update this user to this level'
				}
				return render(request, 'users/manage_user_permissions.html', content)
			
			if newLevelTargetUser == maxLevelTargetUser:
				content = {
							'user': request.user,
							'currentlevel': maxLevelTargetUser,
							'targetuser': targetUser,
							'minlevel': minLevel,
							'maxlevel': maxLevel,
							'page': '2',
							'error_message': 'You have chosen the same level as before'
				}
				return render(request, 'users/manage_user_permissions.html', content)
			
			if newLevelTargetUser < maxLevelTargetUser and not user.is_superuser:
				content = {
							'user': request.user,
							'currentlevel': maxLevelTargetUser,
							'targetuser': targetUser,
							'minlevel': minLevel,
							'maxlevel': maxLevel,
							'page': '2',
							'error_message': 'You cannot remove security levels if you are not the administrator'
				}
				return render(request, 'users/manage_user_permissions.html', content)
			
			if newLevelTargetUser > maxLevelTargetUser:
				
				### Add new levels ###
				
				# Get Java interface
				# Remember to start JVM server first!!!
				gateway = JavaGateway()
				javaObject = gateway.entry_point.getObject()
			
				### Fetch current user keys ###
				
				# Get user credentials
				credential = models.Credential.objects.get(user=user)
				privKey = credential.wrappedPrivKey.read()
				iv = credential.iv.read()
				salt = credential.salt.read()
				
				# Need to use Java arrays objects in order to call java method
				privKeyArray = gateway.new_array(gateway.jvm.byte, len(privKey))
				for i in range(0,len(privKey)):
					privKeyArray[i] = privKey[i]
				
				ivArray = gateway.new_array(gateway.jvm.byte, len(iv))
				for i in range(0,len(iv)):
					ivArray[i] = iv[i]
			
				# Unwrap user private RSA key in order to encrypt new level key
				unwrappedPrivKey = javaObject.unwrapPivateRSAKey(privKeyArray, ivArray, username, password, str(salt))
				
				unwrappedPrivKeyArray = gateway.new_array(gateway.jvm.byte, len(unwrappedPrivKey))
				for i in range(0,len(unwrappedPrivKey)):
					unwrappedPrivKeyArray[i] = unwrappedPrivKey[i]
				
				### Fetch target user public key ###
				
				# Get other user public key
				targetUserCredential = models.Credential.objects.get(user=targetUserObj)
				targetUserPubKey = targetUserCredential.pubKey.read()
				
				targetUserPubKeyArray = gateway.new_array(gateway.jvm.byte, len(targetUserPubKey))
				for i in range(0,len(targetUserPubKey)):
					targetUserPubKeyArray[i] = targetUserPubKey[i]
				
				### Add levels to target user ###
				
				for l in range(maxLevelTargetUser+1, newLevelTargetUser+1):
					ulk = models.UserLevel.objects.get(user=user, level__levNumber=l)
					
					levWrappedAESKey = ulk.cryptedAESKey.read()
					
					levWrappedAESKeyArray = gateway.new_array(gateway.jvm.byte, len(levWrappedAESKey))
					for i in range(0,len(levWrappedAESKey)):
						levWrappedAESKeyArray[i] = levWrappedAESKey[i]
					
					# Get the AES key for current level
					levAESKey = javaObject.unwrapAESWithRSA(levWrappedAESKeyArray, unwrappedPrivKeyArray)
					
					levAESKeyArray = gateway.new_array(gateway.jvm.byte, len(levAESKey))
					for i in range(0,len(levAESKey)):
						levAESKeyArray[i] = levAESKey[i]
					
					# Wrap AES level key for target user
					newLevEncryptedAESKey = javaObject.wrapAESWithRSA(levAESKeyArray, targetUserPubKeyArray)
					
					# Create and save new object in the database
					newUserLev = models.UserLevel()
					
					newUserLev.user = targetUserObj
					newUserLev.level = ulk.level
					newUserLev.cryptedAESKey.save(targetUserObj.username + str(ulk.level.levNumber), ContentFile(newLevEncryptedAESKey), save=False)
					
					newUserLev.save()
			
			else:
				
				### Deleting levels ###
				
				# Remove all files and objects
				for l in range(newLevelTargetUser+1, maxLevelTargetUser+1):
					u = models.UserLevel.objects.get(user__username=targetUser, level__levNumber=l)
					
					u.cryptedAESKey.delete(save=True)
					u.delete()
			
			content = {
						'user': request.user,
						'userlist': getOtherUsersList(request.user),
						'page': '1',
						'confirm_message': 'Operation completed'
			}
			return render(request, 'users/manage_user_permissions.html', content)

	return HttpResponseRedirect(reverse('users:index'))


# REST interface views

class credentialREST(APIView):
	
	permission_classes = (permissions.IsAuthenticated,)
	
	def get(self, request, format=None):
		
		# Returns personal keyring (RSA and DSA keys) for current logged user
		
		credObj = models.Credential.objects.get(user=request.user)
		
		ser = models.SerializedCredential()
		
		ser.username = request.user.username
		ser.pubKeyBase64 = credObj.pubKey.read().encode('base64')[:-1]
		ser.wrappedPrivKeyBase64 = credObj.wrappedPrivKey.read().encode('base64')[:-1]
		ser.ivBase64 = credObj.iv.read().encode('base64')[:-1]
		ser.saltBase64 = credObj.salt.read().encode('base64')[:-1]
		ser.pubDSAKeyBase64 = credObj.pubDSAKey.read().encode('base64')[:-1]
		ser.wrappedPrivDSAKeyBase64 = credObj.wrappedPrivDSAKey.read().encode('base64')[:-1]
		ser.ivDSABase64 = credObj.ivDSA.read().encode('base64')[:-1]
		ser.saltDSABase64 = credObj.saltDSA.read().encode('base64')[:-1]
		
		serializer = serializers.CredentialSerializer(ser)
		return Response(serializer.data)



class listUserLevelsREST(APIView):
	
	permission_classes = (permissions.IsAuthenticated,)
	
	def get(self, request, format=None):
		
		# Returns a list of all security levels ID current logged user owns
		
		userLevelObjs = models.Level.objects.filter(userlevel__user=request.user)
		
		serializer = serializers.LevelListSerializer(userLevelObjs, many=True)
		return Response(serializer.data)



class userLevelREST(APIView):
	
	permission_classes = (permissions.IsAuthenticated,)
	
	def get(self, request, level, format=None):
		
		# Returns level AES key wrapped with personal user key for current logged user
		
		try:
			userLevObj = models.UserLevel.objects.get(user=request.user, level__levNumber=level)
		except models.UserLevel.DoesNotExist:
			content = {'Error': 'User does not have selected level'}
			return Response(content, status.HTTP_404_NOT_FOUND)
		
		ser = models.SerializedUserLevel()
		
		ser.username = request.user.username
		ser.level = int(level)
		ser.cryptedAESKeyBase64 = userLevObj.cryptedAESKey.read().encode('base64')[:-1]
		
		serializer = serializers.LevelSerializer(ser)
		return Response(serializer.data)



class userDSAPublicKeyREST(APIView):
	
	permission_classes = (permissions.IsAuthenticated,)
	
	def get(self, request, username, format=None):
		
		# Returns user DSA Public Key (if user exists)
		
		try:
			credentialObj = models.Credential.objects.get(user__username=username)
		except models.Credential.DoesNotExist:
			content = {'Error': 'User does not exists'}
			return Response(content, status.HTTP_404_NOT_FOUND)
		
		ser = models.SerializedUserPublicDSAKey()
		
		ser.username = credentialObj.user.username
		ser.pubDSAKeyBase64 = credentialObj.pubDSAKey.read().encode('base64')[:-1]
		
		serializer = serializers.UserDSAKeySerializer(ser)
		return Response(serializer.data)



# Access control views

def login(request):
	if request.method == 'GET':
		if request.user.is_authenticated():
			return HttpResponseRedirect(reverse('users:index'))
		else:
			nexturl = request.GET.get('next', '')
			return render(request, 'users/login.html', {'next': nexturl})
	
	elif request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']
		nexturl = request.POST.get('next', '')
		
		user = django.contrib.auth.authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				django.contrib.auth.login(request, user)
				if nexturl:
					return HttpResponseRedirect(nexturl)
				else:
					return HttpResponseRedirect(reverse('users:index'))
			else:
				return render(request, 'users/login.html', {'error_message': 'Inactive account!'})
		else:
			return render(request, 'users/login.html', {'error_message': 'Invalid credentials!'})



def logout(request):
	if request.user.is_authenticated():
		django.contrib.auth.logout(request)
	return render(request, 'users/logout.html')
