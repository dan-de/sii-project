from django.db import models
from django.conf import settings

# Create your models here.
class Document(models.Model):
	creator = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
	docID = models.IntegerField(unique=True)
	docTitle = models.CharField(max_length=100, blank=True, null=True)
	creation_date = models.DateTimeField(auto_now_add=True)
	last_modify = models.DateTimeField(auto_now=True)
	hashes = models.CharField(max_length=4096, blank=True, null=True)
	
	def __unicode__(self):  # Python 3: def __str__(self):
		return str(self.docID)

class DocPart(models.Model):
	document = models.ForeignKey(Document, on_delete=models.CASCADE)
	level = models.ForeignKey('users.Level', on_delete=models.CASCADE)
	partID = models.IntegerField()
	ciphertext = models.FileField(upload_to='documents/ciphertexts')
	iv = models.FileField(upload_to='documents/iv')
	
	def __unicode__(self):  # Python 3: def __str__(self):
		return str('Part ' + str(self.partID) + ' for document ' + str(self.document))
	
	class Meta:
		unique_together = ('document', 'partID', 'level')



# Serialization classes
# They should be transient class models
class SerializedDocumentPartInfo(models.Model):
	docID = models.IntegerField()
	partID = models.IntegerField()

class SerializedDocumentPart(models.Model):
	docID = models.IntegerField()
	partID = models.IntegerField()
	level = models.IntegerField()
	ciphertext = models.CharField(max_length=4096)
	iv = models.CharField(max_length=1024)
