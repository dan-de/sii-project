# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DocPart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('partID', models.IntegerField()),
                ('ciphertext', models.FileField(upload_to=b'documents/ciphertexts')),
                ('iv', models.FileField(upload_to=b'documents/iv')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('docID', models.IntegerField(unique=True)),
                ('docTitle', models.CharField(max_length=100, null=True, blank=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('last_modify', models.DateTimeField(auto_now=True)),
                ('hashes', models.CharField(max_length=4096, null=True, blank=True)),
                ('creator', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SerializedDocumentPart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('docID', models.IntegerField()),
                ('partID', models.IntegerField()),
                ('level', models.IntegerField()),
                ('ciphertext', models.CharField(max_length=4096)),
                ('iv', models.CharField(max_length=1024)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SerializedDocumentPartInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('docID', models.IntegerField()),
                ('partID', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='docpart',
            name='document',
            field=models.ForeignKey(to='documents.Document'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='docpart',
            name='level',
            field=models.ForeignKey(to='users.Level'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='docpart',
            unique_together=set([('document', 'partID', 'level')]),
        ),
    ]
