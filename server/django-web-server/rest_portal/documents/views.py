from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions

from django.core.files.base import ContentFile

from django.utils.crypto import get_random_string

import base64

from documents import models
from documents import serializers

from users import models as usermodels
from users import serializers as userserializers



def generateSerial():
	chars = '0123456789'
	return int(get_random_string(10, chars))



# REST views
class listUserDocumentsREST(APIView):
	permission_classes = (permissions.IsAuthenticated,)
	
	def get(self, request, format=None):
		
		### List all document created by current logged user ###
		
		userDocList = models.Document.objects.filter(creator=request.user)
		
		serializer = serializers.DocumentSerializer(userDocList, many=True)
		return Response(serializer.data)
	
	def post(self, request, format=None):
		
		### Create a new document ###
		
		serializer = serializers.DocumentSerializer(data=request.DATA)
		
		if not serializer.is_valid():
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		
		# Get docID from request
		docID = serializer.object.docID
		
		# If null generate a new docID
		if docID is None:
			docID = generateSerial()
		
		# Check if docID exists
		while models.Document.objects.filter(docID=docID).exists():
			# Generate a new docID
			docID = generateSerial()
		
		# Put new docID into the received request
		serializer.object.docID = docID
		
		# Put the creator
		serializer.object.creator = request.user
		
		serializer.save()
		
		# Send the received request, but with the docID used to store this document
		# and the correct creator
		return Response(serializer.data, status=status.HTTP_201_CREATED)



class DocumentREST(APIView):
	permission_classes = (permissions.IsAuthenticated,)
	
	def get(self, request, docID, format=None):
		
		# Check if document exists
		try:
			document = models.Document.objects.get(docID=docID)
		except models.Document.DoesNotExist:
			content = {'Error': 'This docID does not exist in the system'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		### Show document details ###
		
		parts = request.GET.get('parts', '')
		if not parts or parts.lower()!='true':
			serializer = serializers.DocumentSerializer(document)
			return Response(serializer.data)
		
		### Show document parts ###
		
		docParts = models.DocPart.objects.filter(document=document).values_list('partID', flat=True).distinct()
		
		# Copy parts in serializable objects
		serializedPartsDesc = []
		for p in docParts:
			ser = models.SerializedDocumentPartInfo()
			ser.docID = document.docID
			ser.partID = p
			
			serializedPartsDesc.append(ser)
		
		serializer = serializers.DocumentPartInfoSerializer(serializedPartsDesc, many=True)
		return Response(serializer.data)
	
	def put(self, request, docID, format=None):
		
		### Modify a document ###
		
		serializer = serializers.DocumentSerializer(data=request.DATA)
		
		if not serializer.is_valid():
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		
		# Check if docID exists
		try:
			doc = models.Document.objects.get(docID=docID)
		except models.Document.DoesNotExist:
			content = {'Error': 'This docID does not exist in the system'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		# Only docTitle can be changed
		doc.docTitle = serializer.object.docTitle
		
		doc.save()
		
		return Response(serializer.data)
	
	def delete(self, request, docID, format=None):
		
		### Remove a document ###
		
		# Get document object
		try:
			doc = models.Document.objects.get(docID=docID)
		except models.Document.DoesNotExist:
			content = {'Error': 'This docID does not exist in the system'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		# Check if user is owner of the document
		if not doc.creator == request.user:
			content = {'Error': 'Cannot remove a document you don\'t own'}
			return Response(content, status=status.HTTP_403_FORBIDDEN)
		
		# Delete all related ciphertext files and objects
		docParts = models.DocPart.objects.filter(document=doc)
		for p in docParts:
			p.ciphertext.delete(save=True)
			p.iv.delete(save=True)
		
		doc.delete()
		
		return Response(status=status.HTTP_204_NO_CONTENT)



class DocumentPartREST(APIView):
	permission_classes = (permissions.IsAuthenticated,)
	
	def get(self, request, docID, partID, format=None):
		
		### Retrieve partID of docID ###
		
		# Check if there's at least one object for this partID and this docID
		if not models.DocPart.objects.filter(document__docID=docID, partID=partID).exists():
			content = {'Error': 'This partID for given docID does not exist in the system'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		# If a specific level was not requested, show a list of all levels for this partID and docID
		level = request.GET.get('level', '')
		if not level:
			docPartLevels = usermodels.Level.objects.filter(docpart__document__docID=docID, docpart__partID=partID)
			serializer = userserializers.LevelListSerializer(docPartLevels, many=True)
			return Response(serializer.data)
		
		# Get part object
		try:
			docPart = models.DocPart.objects.get(document__docID=docID, partID=partID, level__levNumber=level)
		except models.DocPart.DoesNotExist:
			content = {'Error': 'This partID for given docID and given security level does not exist in the system'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		# Copy object in a serializable object
		ser = models.SerializedDocumentPart()
		ser.docID = docPart.document.docID
		ser.partID = docPart.partID
		ser.level = docPart.level.levNumber
		ser.ciphertext = docPart.ciphertext.read().encode('base64')[:-1]
		ser.iv = docPart.iv.read().encode('base64')[:-1]
		
		serializer = serializers.DocumentPartSerializer(ser)
		return Response(serializer.data)
	
	def post(self, request, docID, partID, format=None):
		
		### Add a new crypted part for this document ###
		
		serializer = serializers.DocumentPartSerializer(data=request.DATA)
		
		if not serializer.is_valid():
			print(serializer.errors)
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		
		# Check if document exists
		try:
			document = models.Document.objects.get(docID=docID)
		except models.Document.DoesNotExist:
			print('This docID does not exist in the system')
			content = {'Error': 'This docID does not exist in the system'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		# Check if user is the owner of the document
		if not document.creator == request.user:
			print('Cannot add document parts to documents you don\'t own')
			content = {'Error': 'Cannot add document parts to documents you don\'t own'}
			return Response(content, status=status.HTTP_403_FORBIDDEN)
		
		# Check if docID already exists
		if models.DocPart.objects.filter(document__docID=docID, partID=partID, level__levNumber=serializer.object.level).exists():
			print('Duplicate partID for this docID and security level')
			content = {'Error': 'Duplicate partID for this docID and security level'}
			return Response(content, status=status.HTTP_400_BAD_REQUEST)
		
		try:
			level = usermodels.Level.objects.get(levNumber=serializer.object.level)
		except usermodels.Level.DoesNotExist:
			print('Invalid security level number')
			content = {'Error': 'Invalid security level number'}
			return Response(content, status=status.HTTP_400_BAD_REQUEST)
		
		if not usermodels.UserLevel.objects.filter(user=request.user, level=level).exists():
			print('Can\'t add a document part encrypted with a security level you don\'t own')
			content = {'Error': 'Can\'t add a document part encrypted with a security level you don\'t own'}
			return Response(content, status=status.HTTP_400_BAD_REQUEST)
		
		
		# Create DocPart object and save to database
		docPart = models.DocPart()
		docPart.document = document
		docPart.level = level
		docPart.partID = partID
		docPart.ciphertext.save(str(document.docID) + '_' + str(partID) + '_' + str(level), ContentFile(serializer.object.ciphertext.decode('base64')), save=False)
		docPart.iv.save(str(document.docID) + '_' + str(partID) + '_' + str(level), ContentFile(serializer.object.iv.decode('base64')), save=False)
		
		docPart.save()
		
		return Response(serializer.data, status=status.HTTP_201_CREATED)
	
	def delete(self, request, docID, partID, format=None):
		
		### Remove a document part ###
		
		# Get document object
		try:
			doc = models.Document.objects.get(docID=docID)
		except models.Document.DoesNotExist:
			print('This docID does not exist in the system')
			content = {'Error': 'This docID does not exist in the system'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		# Check if user is owner of the document
		if not doc.creator == request.user:
			print('Cannot remove a document part of a document you don\'t own')
			content = {'Error': 'Cannot remove a document part of a document you don\'t own'}
			return Response(content, status=status.HTTP_403_FORBIDDEN)
		
		# Get document part
		docParts = models.DocPart.objects.filter(document=doc, partID=partID)
		if not docParts:
			print('This partID for given docID does not exist in the system')
			content = {'Error': 'This partID for given docID does not exist in the system'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		for p in docParts:
			p.ciphertext.delete(save=True)
			p.iv.delete(save=True)
			p.delete()
		
		return Response(status=status.HTTP_204_NO_CONTENT)
