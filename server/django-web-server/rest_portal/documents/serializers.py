from rest_framework import serializers
from documents import models
from django.conf import settings
		
class DocumentSerializer(serializers.ModelSerializer):
	creator = serializers.CharField(source='creator.username', read_only=True)
	
	class Meta:
		model = models.Document
		fields = ('creator', 'docID', 'docTitle', 'creation_date', 'last_modify', 'hashes')
		read_only_fields = ('docID', 'creation_date', 'last_modify')

class DocumentPartInfoSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.SerializedDocumentPartInfo
		fields = ('docID', 'partID')
		read_only_fields = ('docID', 'partID')

class DocumentPartSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.SerializedDocumentPart
		fields = ('docID', 'partID', 'level', 'ciphertext', 'iv')
		read_only_fields = ('docID', 'partID',)
