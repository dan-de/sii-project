from django.conf.urls import patterns, url, include

from documents import views

urlpatterns = patterns('',

	# REST interface for documents management
    url(r'^$', views.listUserDocumentsREST.as_view(), name='listuserdocuments'),
    url(r'^(?P<docID>[0-9]*)/$', views.DocumentREST.as_view(), name='documentrest'),
    url(r'^(?P<docID>[0-9]*)/(?P<partID>[0-9]*)/$', views.DocumentPartREST.as_view(), name='documentpartrest'),
    
)
