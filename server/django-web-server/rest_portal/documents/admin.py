from django.contrib import admin
from documents import models

# Register your models here.
class DocumentAdmin(admin.ModelAdmin):
	fieldsets = [
		('',			{'fields': ['creator', 'docID', 'docTitle', 'creation_date', 'last_modify', 'hashes']}),
	]
	
	readonly_fields = ['creator', 'docID', 'docTitle', 'creation_date', 'last_modify', 'hashes']
	
	list_display = ['pk', 'creator', 'docID', 'docTitle']
	search_fields = ['pk', 'creator', 'docID', 'docTitle']
	ordering = ['creator', 'docID', 'docTitle']

admin.site.register(models.Document, DocumentAdmin)



class DocPartAdmin(admin.ModelAdmin):
	fieldsets = [
		('',				{'fields': ['document', 'level', 'partID', 'ciphertext', 'iv']}),
	]
	
	readonly_fields = ['document', 'level', 'partID', 'ciphertext', 'iv']
	
	list_display = ['pk', 'document', 'level', 'partID']
	search_fields = ['pk', 'document', 'level', 'partID']
	ordering = ['document', 'level', 'partID']

admin.site.register(models.DocPart, DocPartAdmin)
