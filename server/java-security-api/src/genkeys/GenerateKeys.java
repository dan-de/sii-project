package genkeys;

import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class GenerateKeys {
	public static final String AES = "AES";
	public static final String RSA = "RSA";
	public static final String DSA = "DSA";
	public static final String RSA_CIPHER = "RSA";
	public static final String DSA_CIPHER = "SHA1withDSA";
	public static final String AES_CIPHER = "AES/CBC/PKCS5Padding";
	
	public static final int AES_LEN = 128;
	public static final int RSA_LEN = 2048;
	public static final int DSA_LEN = 1024;
	
	PublicKey pubKey = null;
	PrivateKey privKey = null;
	SecretKey aesKey = null;
	PublicKey dsaPubKey = null;
	PrivateKey dsaPrivKey = null;
	
	public GenerateKeys() {
		System.out.println("GenerateKeys.");
	}
	
	public void renewRSA() throws NoSuchAlgorithmException {
		System.out.println("Generating new RSA keys.");
		
		KeyPair keys = RSAGenerator();
		
		pubKey = keys.getPublic();
		privKey = keys.getPrivate();

		System.out.println("Public key len: " + pubKey.getEncoded().length);
		System.out.println("Private key len: " + privKey.getEncoded().length);
	}
	
	public void renewAES() throws NoSuchAlgorithmException {
		aesKey = AESGenerator();

		System.out.println("AES key len: " + aesKey.getEncoded().length);
	}
	
	public void renewDSA() throws NoSuchAlgorithmException {
		System.out.println("Generating new DSA keys.");
		
		KeyPair keys = DSAGenerator();
		
		dsaPubKey = keys.getPublic();
		dsaPrivKey = keys.getPrivate();

		System.out.println("Public key len: " + pubKey.getEncoded().length);
		System.out.println("Private key len: " + privKey.getEncoded().length);
	}
	
	public byte[] getPublicKey() throws NoSuchAlgorithmException {
		System.out.println("Required RSA public key.");
		
		if(pubKey == null) {
			renewRSA();
		}
		
		return pubKey.getEncoded();
	}
	
	public byte[] getPrivateKey() throws NoSuchAlgorithmException {
		System.out.println("Required RSA private key.");
		
		if(privKey == null) {
			renewRSA();
		}
		
		return privKey.getEncoded();
	}
	
	public byte[][] getWrappedRSAPrivKey(String user, String password, String salt) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, InvalidParameterSpecException {
		return wrapPrivateRSAKey(privKey.getEncoded(), user, password, salt);
	}
	
	public byte[] getAESKey() throws NoSuchAlgorithmException {
		System.out.println("Required AES key.");
		
		if(aesKey == null) {
			renewAES();
		}
		
		return aesKey.getEncoded();
	}
	
	public byte[] getDSAPublicKey() throws NoSuchAlgorithmException {
		System.out.println("Required DSA public key.");
		
		if(dsaPubKey == null) {
			renewDSA();
		}
		
		return dsaPubKey.getEncoded();
	}
	
	public byte[] getDSAPrivateKey() throws NoSuchAlgorithmException {
		System.out.println("Required DSA private key.");
		
		if(dsaPrivKey == null) {
			renewDSA();
		}
		
		return dsaPrivKey.getEncoded();
	}
	
	public byte[][] getWrappedDSAPrivKey(String user, String password, String salt) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, InvalidParameterSpecException {
		return wrapPrivateRSAKey(dsaPrivKey.getEncoded(), user, password, salt);
	}
	
	public String getPublicKey64() throws NoSuchAlgorithmException {
		System.out.println("Required public key in Base64.");
		return encodeBase64(getPublicKey());
	}
	
	public String getPrivateKey64() throws NoSuchAlgorithmException {
		System.out.println("Required private key in Base64.");
		return encodeBase64(getPrivateKey());
	}
	
	public String getAESKey64() throws NoSuchAlgorithmException {
		System.out.println("Required AES key in Base64.");
		return encodeBase64(getAESKey());
	}
	
	public String getDSAPublicKey64() throws NoSuchAlgorithmException {
		System.out.println("Required DSA public key in Base64.");
		return encodeBase64(getDSAPublicKey());
	}
	
	public String getDSAPrivateKey64() throws NoSuchAlgorithmException {
		System.out.println("Required DSA private key in Base64.");
		return encodeBase64(getDSAPrivateKey());
	}
	
	/* STATIC METHODS */
	
	public static String encodeBase64(byte[] data) {
		return DatatypeConverter.printBase64Binary(data);
	}
	
	public static byte[] decodeBase64(String data) {
		return DatatypeConverter.parseBase64Binary(data);
	}
	
	public static byte[][] wrapPrivateRSAKey(byte[] keyToWrap, String user, String password, String salt) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, InvalidParameterSpecException {
		byte[][] data = new byte[2][];
		
		SecretKey key = getPassword(user, password, salt);
		
		Cipher cipher = Cipher.getInstance(AES_CIPHER);
		cipher.init(Cipher.WRAP_MODE, key);
		AlgorithmParameters param = cipher.getParameters();
		byte[] iv = param.getParameterSpec(IvParameterSpec.class).getIV();
		
		SecretKey unwrappedKey = new SecretKeySpec(keyToWrap, RSA);
		
		byte[] ciphertext = cipher.wrap(unwrappedKey);

		data[0] = iv;
		data[1] = ciphertext;
		
		return data;
	}
	
	public static byte[] unwrapPivateRSAKey(byte[] wrappedKey, byte[] iv, String user, String password, String salt) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		SecretKey key = getPassword(user, password, salt);
		
		Cipher cipher = Cipher.getInstance(AES_CIPHER);
		cipher.init(Cipher.UNWRAP_MODE, key, new IvParameterSpec(iv));
		
		PrivateKey unwrappedKey = (PrivateKey) cipher.unwrap(wrappedKey, RSA, Cipher.PRIVATE_KEY);
		
		return unwrappedKey.getEncoded();
	}
	
	public static byte[][] cryptDataWithAES(byte[] message, byte[] AESKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidParameterSpecException {
		byte[][] data = new byte[2][];
		
		SecretKey key = new SecretKeySpec(AESKey, AES);
		
		Cipher cipher = Cipher.getInstance(AES_CIPHER);
		cipher.init(Cipher.ENCRYPT_MODE, key);
		AlgorithmParameters param = cipher.getParameters();
		byte[] iv = param.getParameterSpec(IvParameterSpec.class).getIV();
		
		byte[] ciphertext = cipher.doFinal(message);
		
		data[0] = iv;
		data[1] = ciphertext;
		
		return data;
	}
	
	public static byte[] decryptDataWithAES(byte[] ciphertext, byte[] iv, byte[] AESKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
		SecretKey key = new SecretKeySpec(AESKey, AES);
		
		Cipher cipher = Cipher.getInstance(AES_CIPHER);
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
		
		return cipher.doFinal(ciphertext);
	}
	
	public static byte[] wrapAESWithRSA(byte[] AESKey, byte[] RSAPubKey) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		KeyFactory kf = KeyFactory.getInstance(RSA);
		PublicKey key = kf.generatePublic(new X509EncodedKeySpec(RSAPubKey));
		
		SecretKey unwrappedKey = new SecretKeySpec(AESKey, AES);
		
		Cipher cipher = Cipher.getInstance(RSA_CIPHER);
		cipher.init(Cipher.WRAP_MODE, key);
		
		return cipher.wrap(unwrappedKey);
	}
	
	public static byte[] unwrapAESWithRSA(byte[] wrappedAESKey, byte[] RSAPrivKey) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException {
		KeyFactory kf = KeyFactory.getInstance(RSA);
		PrivateKey key = kf.generatePrivate(new PKCS8EncodedKeySpec(RSAPrivKey));
		
		Cipher cipher = Cipher.getInstance(RSA_CIPHER);
		cipher.init(Cipher.UNWRAP_MODE, key);
		
		SecretKey unwrappedKey = (SecretKey) cipher.unwrap(wrappedAESKey, AES, Cipher.SECRET_KEY);
		
		return unwrappedKey.getEncoded();
	}
	
	public static byte[][] wrapPrivateDSAKey(byte[] keyToWrap, String user, String password, String salt) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, InvalidParameterSpecException {
		byte[][] data = new byte[2][];
		
		SecretKey key = getPassword(user, password, salt);
		
		Cipher cipher = Cipher.getInstance(AES_CIPHER);
		cipher.init(Cipher.WRAP_MODE, key);
		AlgorithmParameters param = cipher.getParameters();
		byte[] iv = param.getParameterSpec(IvParameterSpec.class).getIV();
		
		SecretKey unwrappedKey = new SecretKeySpec(keyToWrap, DSA);
		
		byte[] ciphertext = cipher.wrap(unwrappedKey);

		data[0] = iv;
		data[1] = ciphertext;
		
		return data;
	}
	
	public static byte[] unwrapPivateDSAKey(byte[] wrappedKey, byte[] iv, String user, String password, String salt) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		SecretKey key = getPassword(user, password, salt);
		
		Cipher cipher = Cipher.getInstance(AES_CIPHER);
		cipher.init(Cipher.UNWRAP_MODE, key, new IvParameterSpec(iv));
		
		PrivateKey unwrappedKey = (PrivateKey) cipher.unwrap(wrappedKey, DSA, Cipher.PRIVATE_KEY);
		
		return unwrappedKey.getEncoded();
	}
	
	public static byte[] signDataWithDSA(byte[] data, byte[] DSAPrivKey) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException {		
		KeyFactory kf = KeyFactory.getInstance(DSA);
		PrivateKey key = kf.generatePrivate(new PKCS8EncodedKeySpec(DSAPrivKey));
		
		Signature dsa = Signature.getInstance(DSA_CIPHER);
		dsa.initSign(key);
		
		dsa.update(data);

		return dsa.sign();
	}
	
	public static boolean veriyDataSignWithDSA(byte[] data, byte[] sign, byte[] DSAPublicKey) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException {
		KeyFactory kf = KeyFactory.getInstance(DSA);
		PublicKey key = kf.generatePublic(new X509EncodedKeySpec(DSAPublicKey));
		
		Signature dsa = Signature.getInstance(DSA_CIPHER);
		dsa.initVerify(key);
		
		dsa.update(data);
		
		return dsa.verify(sign);
	}
	
	/* PRIVATE METHODS */
	
	private static SecretKeySpec getPassword(String user, String password, String salt) throws NoSuchAlgorithmException {
		byte[] key = (user + password + salt).getBytes();
		MessageDigest sha = MessageDigest.getInstance("SHA-1");
		key = sha.digest(key);
		key = Arrays.copyOf(key, 16);
		
		return new SecretKeySpec(key, AES);
	}
	
	private static KeyPair RSAGenerator() throws NoSuchAlgorithmException {
		KeyPairGenerator kp = KeyPairGenerator.getInstance(RSA);
		kp.initialize(RSA_LEN);
		return kp.generateKeyPair();
	}
	
	private static SecretKey AESGenerator() throws NoSuchAlgorithmException {
		
		KeyGenerator keyGen = KeyGenerator.getInstance(AES);
		keyGen.init(AES_LEN); // 128bit
		SecretKey secretKey = keyGen.generateKey();
		
		return secretKey;
	}
	
	private static KeyPair DSAGenerator() throws NoSuchAlgorithmException {
		KeyPairGenerator kp = KeyPairGenerator.getInstance(DSA);
		kp.initialize(DSA_LEN);
		return kp.generateKeyPair();
	}
}
