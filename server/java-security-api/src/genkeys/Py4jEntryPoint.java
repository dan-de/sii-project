package genkeys;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.Arrays;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import py4j.GatewayServer;

public class Py4jEntryPoint {
	
	public Py4jEntryPoint() {
		
	}

	public GenerateKeys getObject() {
		System.out.println("Required object from Java gateway server.");
		return new GenerateKeys();
	}
	
	public static void main(String[] args) {
		if (!testCiphers())
			return;
		
		GatewayServer gatewayServer = new GatewayServer(new Py4jEntryPoint());
		gatewayServer.start();
		System.out.println("Gateway Server Started");
	}

	public static boolean testCiphers() {
		GenerateKeys genk = new GenerateKeys();
		
		System.out.println("#i# Generating certificates.");
		byte[] pubKey;
		byte[] privKey;
		byte[] secretKey;
		byte[] pubDSAKey;
		byte[] privDSAKey;
		try {
			pubKey = genk.getPublicKey();
			privKey = genk.getPrivateKey();
			secretKey = genk.getAESKey();
			pubDSAKey = genk.getDSAPublicKey();
			privDSAKey = genk.getDSAPrivateKey();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return false;
		}
		
		System.out.println("#i# Testing Base64 encoding.");
		try {
			String privKey64 = genk.getPrivateKey64();
			String pubKey64 = genk.getPublicKey64();
			String secretKey64 = genk.getAESKey64();
			
			if ( !(Arrays.equals(pubKey, GenerateKeys.decodeBase64(pubKey64)) &&
				Arrays.equals(privKey, GenerateKeys.decodeBase64(privKey64)) &&
				Arrays.equals(secretKey, GenerateKeys.decodeBase64(secretKey64))) ) {
				
				System.out.println("#E# Failed to use Base64 encoding.");
				return false;
				
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return false;
		}
		
		System.out.println("#i# Testing RSA wrapping.");
		try {
			byte[][] wrappedSecretKey = GenerateKeys.wrapPrivateRSAKey(privKey, "user1", "password1", "salt salt");
			if (!Arrays.equals(GenerateKeys.unwrapPivateRSAKey(wrappedSecretKey[1], wrappedSecretKey[0], "user1", "password1", "salt salt"), privKey)) {
				
				System.out.println("#E# Failed to wrap/unwrap RSA private key.");
				return false;
				
			}
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | InvalidAlgorithmParameterException
				| IllegalBlockSizeException | InvalidParameterSpecException e) {
			e.printStackTrace();
			return false;
		}

		System.out.println("#i# Testing AES crypting.");
		try {
			byte[] testdata = new byte[1024];
			Random r = new Random();
			for (int i=0; i<1024; i++)
				testdata[i] = (byte) r.nextInt(256);
			
			byte[][] ciphertext = GenerateKeys.cryptDataWithAES(testdata, secretKey);
			
			if (!Arrays.equals(GenerateKeys.decryptDataWithAES(ciphertext[1], ciphertext[0], secretKey), testdata)) {
				
				System.out.println("#E# Failed to crypt with AES.");
				return false;
				
			}
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | InvalidParameterSpecException | InvalidAlgorithmParameterException e) {
			e.printStackTrace();
			return false;
		}

		System.out.println("#i# Testing AES key wrapping.");
		try {
			byte[] wrappedAESKey = GenerateKeys.wrapAESWithRSA(secretKey, pubKey);
			
			if (!Arrays.equals(GenerateKeys.unwrapAESWithRSA(wrappedAESKey, privKey), secretKey)) {

				System.out.println("#E# Failed to wrap AES key with RSA.");
				return false;
				
			}
			
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| InvalidKeySpecException | NoSuchPaddingException
				| IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
			return false;
		}
		
		System.out.println("#i# Testing DSA wrapping.");
		try {
			byte[][] wrappedSecretDSAKey = GenerateKeys.wrapPrivateDSAKey(privDSAKey, "user1", "password1", "salt salt");
			if (!Arrays.equals(GenerateKeys.unwrapPivateDSAKey(wrappedSecretDSAKey[1], wrappedSecretDSAKey[0], "user1", "password1", "salt salt"), privDSAKey)) {
				
				System.out.println("#E# Failed to wrap/unwrap DSA private key.");
				return false;
				
			}
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | InvalidAlgorithmParameterException
				| IllegalBlockSizeException | InvalidParameterSpecException e) {
			e.printStackTrace();
			return false;
		}

		System.out.println("#i# Testing DSA signature.");
		try {
			byte[] testdata = new byte[1024];
			Random r = new Random();
			for (int i=0; i<1024; i++)
				testdata[i] = (byte) r.nextInt(256);
			
			byte[] sign = GenerateKeys.signDataWithDSA(testdata, privDSAKey);
			
			if (!GenerateKeys.veriyDataSignWithDSA(testdata, sign, pubDSAKey)) {

				System.out.println("#E# Failed to verify DSA signed data.");
				return false;
				
			}
			
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| InvalidKeySpecException | SignatureException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
}
