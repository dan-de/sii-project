# Server for SII Project

## Prerequisites

Server application is written in Python and uses gateway connection to some
Java functions. You need Python 2.7 and Java 7 JDK installed.

### Java versions
Unlike Client part of this project, the server doesn't require the Oracle
version of Java. You can use OpenJDK 7. Under Ubuntu you can install it with:

		~# apt-get install openjdk-7-jdk

Pay attention that if you use Oracle JDK you must install the **JCE Policy**.
With Ubuntu you can do this installing *oracle-java7-unlimited-jce-policy*:

		~# apt-get install oracle-java7-unlimited-jce-policy

### Python dependecies
Django Framework runs under a virtual environment. You need these packages to
let the environment creation: python-virtualenv python-pip

		~# apt-get install python-virtualenv python-pip

## Installing environment

First of all copy the Django local settings example file:

		~$ cp django-web-server/rest_portal/rest_portal/local_settings.py.example \
		~$ django-web-server/rest_portal/rest_portal/local_settings.py

Modify the variables as you need. In particular, during debug process, you probably
want to change **MEDIA_ROOT** path into a directory your user can write without
root permissions.

Run the *create-environment.sh* script under the *django-web-server* folder to
initialize the server:

		~$ django-web-server/create-environment.sh

This should be enough, but you can run the Java gateway project from Eclipse
importing the *java-security-api* Eclipse folder.
Note that this project depends on an external library that *create-environment.sh*
downloads and put under the Eclipse folder for you. If you see syntax errors in
Eclipse, probably you forgot to run the script.

## Running the server

To run the server simply execute the *run-server.sh* script in *django-web-server*
folder:

		~$ django-web-server/run-server.sh

## Deploy in Apache

First install Apache:

		~# apt-get install apache2 libapache2-mod-wsgi

Then, assuming your Apache *DocumentRoot* folder is under */var/www* run these commands
as root user:

```
#!bash
		mkdir /var/www/static
		mkdir /var/www/media
		mkdir /var/www/java-security-api/
		cp -r django-web-server/ /var/www/
		cp java-security-api/gateway.jar /var/www/java-security-api/
		cd /var/www/django-web-server/
		cp rest_portal/rest_portal/local_settings.py.example rest_portal/rest_portal/local_settings.py
		./create-environment.sh
		./update-static.sh
		rm -f /etc/apache2/sites-enabled/000-default.conf
		cp 000-django-server.conf /etc/apache2/sites-enabled/
		a2enmod ssl
		chown -R www-data:www-data /var/www
		service apache2 restart
		
		java -jar ../java-security-api/gateway.jar &
		
```

Remember to execute last line every time you use the server. It enables the Java Gateway interface used by
Django for some operations.

Optionally change the Apache default self-signed certificates with new ones:

		~# openssl req -new -x509 -days 365 -nodes -out /etc/ssl/certs/django.pem -keyout /etc/ssl/private/django.key

and then change the paths of these files in */etc/apache2/sites-enabled/000-django-server.conf* file you previously
copied. In particular change **SSLCertificateFile** and **SSLCertificateKeyFile** directives.
