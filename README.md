# SII Project

By Daniele De Angelis, Stefano Di Francescangelo and Federico Cosentino
(C) 2014

------------------------------------------------------------------------

This project is made of two parts. A Python server and a Java client with
a GUI interface. Check specific README files for Client and Server.
