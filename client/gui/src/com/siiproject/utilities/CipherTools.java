package com.siiproject.utilities;

import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CipherTools {
	public static final String AES = "AES";
	public static final String RSA = "RSA";
	public static final String DSA = "DSA";
	public static final String RSA_CIPHER = "RSA";
	public static final String DSA_CIPHER = "SHA1withDSA";
	public static final String AES_CIPHER = "AES/CBC/PKCS5Padding";
	
	private static CipherTools instance = null;
	
	private ConnectionUtility connection;
	
	private byte[] RSAPrivKey;
	private byte[] RSAiv;
	private byte[] RSAsalt;
	
	private byte[] DSAPrivKey;
	private byte[] DSAiv;
	private byte[] DSAsalt;
	
	private Map<Integer, byte[]> userLevels;
	
	private CipherTools(ConnectionUtility u) throws CipherToolsException, JSONException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException {
		connection = u;
		if (connection == null) {
			throw new CipherToolsException("WARNING: connection object was null.");
		}
		
		if (!connection.isConnected()) {
			throw new CipherToolsException("WARNING: not connected to the server.");
		}
		
		
		
		/* Request certificates from server */
		
		System.out.println("Requesting personal certificates...");
		
		String cred = connection.getJSONCredentials();
		System.out.println("Received JSON: " + cred);
		JSONObject json = new JSONObject(cred);
		
		RSAiv = decodeBase64(json.getString("ivBase64"));
		RSAsalt = decodeBase64(json.getString("saltBase64"));
		
		DSAiv = decodeBase64(json.getString("ivDSABase64"));
		DSAsalt = decodeBase64(json.getString("saltDSABase64"));
		
		byte[] wrappedRSA = decodeBase64(json.getString("wrappedPrivKeyBase64"));
		RSAPrivKey = unwrapPivateRSAKey(wrappedRSA, RSAiv, u.username, u.password, new String(RSAsalt));
		
		byte[] wrappedDSA = decodeBase64(json.getString("wrappedPrivDSAKeyBase64"));
		DSAPrivKey = unwrapPivateDSAKey(wrappedDSA, DSAiv, u.username, u.password, new String(DSAsalt));
		
		
		
		/* Request AES key for security levels */
		
		userLevels = new HashMap<Integer, byte[]>();
		
		String levList = connection.getJSONUserLevelList();
		System.out.println("Received JSON: " + levList);
		JSONArray levArray = new JSONArray(levList);
		
		for (int i = 0; i< levArray.length(); i++) {
			JSONObject jLevNumber = levArray.getJSONObject(i);
			int levNum = jLevNumber.getInt("levNumber");
			
			String lev = connection.getJSONUserLevel(levNum);
			System.out.println("Received JSON: " + lev);
			
			JSONObject jLevKey = new JSONObject(lev);
			byte[] cryptedAESKey = decodeBase64(jLevKey.getString("cryptedAESKeyBase64"));
			byte[] AESKey = unwrapAESWithRSA(cryptedAESKey, RSAPrivKey);
			
			userLevels.put(levNum, AESKey);
		}
	}
	
	public static CipherTools getInstance(ConnectionUtility u) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException, CipherToolsException, JSONException {
		if (instance == null)
			instance = new CipherTools(u);
		return instance;
	}
	
	public static void destroyInstance() {
		System.out.println("Destroying user keys.");
		instance = null;
	}
	
	public Set<Integer> getLevels() {
		return userLevels.keySet();
	}
	
	public byte[] getLevel(int level) {
		return userLevels.get(level);
	}
	
	public byte[][] encryptWithLevel(int level, byte[] message) throws CipherToolsException {
		byte[] aes = userLevels.get(level);
		if (aes == null)
			throw new CipherToolsException("No such level for current user.");
		
		try {
			
			return cryptDataWithAES(message, aes);
			
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | InvalidParameterSpecException e) {
			throw new CipherToolsException("Internal error: unable to encrypt data.");
		}
	}
	
	public byte[] decryptWithLevel(int level, byte[] ciphertext, byte[] iv) throws CipherToolsException {
		byte[] aes = userLevels.get(level);
		if (aes == null)
			throw new CipherToolsException("No such level for current user.");
		
		try {
			return decryptDataWithAES(ciphertext, iv, aes);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | InvalidAlgorithmParameterException e) {
			throw new CipherToolsException("Internal error: unable to decrypt data.");
		}
	}
	
	public byte[] signMessage(byte[] message) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, SignatureException {
		return signDataWithDSA(message, DSAPrivKey);
	}

	public boolean verifyMessage(byte[] message, byte[] sign, String username) throws JSONException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, SignatureException {
		String dsajson = connection.getJSONUserDSAPubKey(username);
		JSONObject json = new JSONObject(dsajson);
		
		byte[] DSAPubKey = decodeBase64(json.getString("pubDSAKeyBase64"));
		
		return veriyDataSignWithDSA(message, sign, DSAPubKey);
	}
	
	/* STATIC METHODS */
	
	public static String encodeBase64(byte[] data) {
		return DatatypeConverter.printBase64Binary(data);
	}
	
	public static byte[] decodeBase64(String data) {
		return DatatypeConverter.parseBase64Binary(data);
	}
	
	public static byte[] unwrapPivateRSAKey(byte[] wrappedKey, byte[] iv, String user, String password, String salt) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		SecretKey key = getPassword(user, password, salt);
		
		Cipher cipher = Cipher.getInstance(AES_CIPHER);
		cipher.init(Cipher.UNWRAP_MODE, key, new IvParameterSpec(iv));
		
		PrivateKey unwrappedKey = (PrivateKey) cipher.unwrap(wrappedKey, RSA, Cipher.PRIVATE_KEY);
		
		return unwrappedKey.getEncoded();
	}
	
	public static byte[][] cryptDataWithAES(byte[] message, byte[] AESKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidParameterSpecException {
		byte[][] data = new byte[2][];
		
		SecretKey key = new SecretKeySpec(AESKey, AES);
		
		Cipher cipher = Cipher.getInstance(AES_CIPHER);
		cipher.init(Cipher.ENCRYPT_MODE, key);
		AlgorithmParameters param = cipher.getParameters();
		byte[] iv = param.getParameterSpec(IvParameterSpec.class).getIV();
		
		byte[] ciphertext = cipher.doFinal(message);
		
		data[0] = iv;
		data[1] = ciphertext;
		
		return data;
	}
	
	public static byte[] decryptDataWithAES(byte[] ciphertext, byte[] iv, byte[] AESKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
		SecretKey key = new SecretKeySpec(AESKey, AES);
		
		Cipher cipher = Cipher.getInstance(AES_CIPHER);
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
		
		return cipher.doFinal(ciphertext);
	}
	
	public static byte[] unwrapAESWithRSA(byte[] wrappedAESKey, byte[] RSAPrivKey) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException {
		KeyFactory kf = KeyFactory.getInstance(RSA);
		PrivateKey key = kf.generatePrivate(new PKCS8EncodedKeySpec(RSAPrivKey));
		
		Cipher cipher = Cipher.getInstance(RSA_CIPHER);
		cipher.init(Cipher.UNWRAP_MODE, key);
		
		SecretKey unwrappedKey = (SecretKey) cipher.unwrap(wrappedAESKey, AES, Cipher.SECRET_KEY);
		
		return unwrappedKey.getEncoded();
	}
	
	public static byte[] unwrapPivateDSAKey(byte[] wrappedKey, byte[] iv, String user, String password, String salt) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		
		SecretKey key = getPassword(user, password, salt);
		
		Cipher cipher = Cipher.getInstance(AES_CIPHER);
		cipher.init(Cipher.UNWRAP_MODE, key, new IvParameterSpec(iv));
		
		PrivateKey unwrappedKey = (PrivateKey) cipher.unwrap(wrappedKey, DSA, Cipher.PRIVATE_KEY);
		
		return unwrappedKey.getEncoded();
	}
	
	public static byte[] signDataWithDSA(byte[] data, byte[] DSAPrivKey) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException {		
		KeyFactory kf = KeyFactory.getInstance(DSA);
		PrivateKey key = kf.generatePrivate(new PKCS8EncodedKeySpec(DSAPrivKey));
		
		Signature dsa = Signature.getInstance(DSA_CIPHER);
		dsa.initSign(key);
		
		dsa.update(data);

		return dsa.sign();
	}
	
	public static boolean veriyDataSignWithDSA(byte[] data, byte[] sign, byte[] DSAPublicKey) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException {
		KeyFactory kf = KeyFactory.getInstance(DSA);
		PublicKey key = kf.generatePublic(new X509EncodedKeySpec(DSAPublicKey));
		
		Signature dsa = Signature.getInstance(DSA_CIPHER);
		dsa.initVerify(key);
		
		dsa.update(data);
		
		return dsa.verify(sign);
	}
	
	/* PRIVATE METHODS */
	
	private static SecretKeySpec getPassword(String user, String password, String salt) throws NoSuchAlgorithmException {
		byte[] key = (user + password + salt).getBytes();
		MessageDigest sha = MessageDigest.getInstance("SHA-1");
		key = sha.digest(key);
		key = Arrays.copyOf(key, 16);
		
		return new SecretKeySpec(key, AES);
	}
}
