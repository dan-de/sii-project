package com.siiproject.utilities;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javafx.util.Pair;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DocumentTools {
	private ConnectionUtility connection;
	private CipherTools cipher;
	
	private String title;
	/*               partID   Level     ciphertext */
	private Map<Pair<Integer, Integer>, byte[][]> encryptedParts;
	private long docID;
	private String hashes;
	
	public DocumentTools(ConnectionUtility u, CipherTools c) throws DocumentToolsException {
		connection = u;
		cipher = c;
		
		title = "";
		encryptedParts = new HashMap<Pair<Integer, Integer>, byte[][]>();
		docID = -1;
		hashes = "";
	}
	
	
	
	/* GENERAL METHODS */
	
	/**
	 * @return a set of levels this user is authorized to use
	 */
	public Set<Integer> getLevels() {
		return cipher.getLevels();
	}
	
	
	
	/* GET EXISTING DOCUMENT */
	
	/**
	 * @param docID
	 * @param partID
	 * @param level
	 * @return the decrypted part or null
	 */
	public String getPart(long docID, int partID, int level) {
		System.out.println("getPart() method for docID=" + docID + " partID=" + partID + " and level=" + level + ".");
		
		String result;
		
		byte[] AES = cipher.getLevel(level);
		if (AES == null)
			return null;
		
		String jsonString = connection.getJSONDocPartCiphertext(docID, partID, level);
		
		if (jsonString.length() == 0)
			return null;
		
		try {
			
			JSONObject json = new JSONObject(jsonString);
			
			byte[] ciphertext = CipherTools.decodeBase64(json.getString("ciphertext"));
			byte[] iv = CipherTools.decodeBase64(json.getString("iv"));
			
			result = new String(CipherTools.decryptDataWithAES(ciphertext, iv, AES));
			
		} catch (JSONException e) {
			
			System.out.println("Invalid response from server.");
			return null;
			
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | InvalidAlgorithmParameterException e) {
			
			System.out.println("Cannot decode this ciphertext.");
			return null;
		}
		
		return result;
	}
	
	/**
	 * @param docID
	 * @param partID
	 * @return the decrypted part or null
	 */
	public String getPart(long docID, int partID) {
		System.out.println("getPart() method for docID=" + docID + "and partID=" + partID + ".");
		
		Integer level = getAvailableLevelForPart(docID, partID);
		if (level == null)
			return null;
		
		return getPart(docID, partID, level);
	}

	/**
	 * @param docID
	 * @param partID
	 * @return a level this user is authorized to use to decrypt this partID of this docID
	 */
	public Integer getAvailableLevelForPart(long docID, int partID) {
		System.out.println("getAvailableLevelForPart() method for docID=" + docID + "and partID=" + partID + ".");
		
		String jsonString = connection.getJSONDocPartLevelList(docID, partID);
		
		if (jsonString.length() == 0)
			return null;
		
		try {
			
			JSONArray jsonArray = new JSONArray(jsonString);
			
			for (int i = 0; i< jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				int level = json.getInt("levNumber");
				
				if (cipher.getLevel(level)!=null)
					return level;
			}
			
		} catch (JSONException e) {
			
			System.out.println("Invalid response from server.");
			return null;
			
		}
		
		System.out.println("No level can decrypt requested part.");
		return null;
	}
	
	/**
	 * @param docID
	 * @return username of the creator of this document
	 */
	public String getCreator(long docID) {
		
		try {
			JSONObject json = getDetails(docID);
			
			if (json == null)
				return null;
			
			return json.getString("creator");
			
		} catch (JSONException e) {
			System.out.println("Invalid response from server.");
			return null;
		}
		
	}

	/**
	 * @param docID
	 * @return title of this document or null if not defined
	 */
	public String getTitle(long docID) {
		
		try {
			JSONObject json = getDetails(docID);
			
			if (json == null)
				return null;
			
			return json.getString("docTitle");
			
		} catch (JSONException e) {
			System.out.println("Invalid response from server. Probably missing document title.");
			return "None";
		}
		
	}
	
	/**
	 * @param docID
	 * @return date of creation of this document expressed according to SQL format
	 */
	public String getCreationDate(long docID) {
		
		try {
			JSONObject json = getDetails(docID);
			
			if (json == null)
				return null;
			
			return json.getString("creation_date");
			
		} catch (JSONException e) {
			System.out.println("Invalid response from server.");
			return null;
		}
		
	}

	/**
	 * @param docID
	 * @return date of last modify of this document expressed according to SQL format
	 */
	public String getLastModifyDate(long docID) {
		
		try {
			JSONObject json = getDetails(docID);
			
			if (json == null)
				return null;
			
			return json.getString("last_modify");
			
		} catch (JSONException e) {
			System.out.println("Invalid response from server.");
			return null;
		}
		
	}

	/**
	 * @param docID
	 * @return hashes of the document
	 */
	public String getHashes(long docID) {
		
		try {
			JSONObject json = getDetails(docID);
			
			if (json == null)
				return null;
			
			return json.getString("hashes");
			
		} catch (JSONException e) {
			System.out.println("Invalid response from server.");
			return null;
		}
		
	}
	
	
	
	/* CREATE NEW DOCUMENT */

	/**
	 * Set a title for this document (optional field)
	 * @param t title
	 */
	public void setTitle(String t) {
		title = t;
	}
	
	/**
	 * Add a part to be encrypted and to be sent to the server with submit() method
	 * @param partID
	 * @param level
	 * @param message
	 * @throws DocumentToolsException if an error occurred
	 */
	public void addPart(int partID, int level, String message) throws DocumentToolsException {
		System.out.println("addPart() method for partID=" + partID + " level=" + level + " and message=\"" + message + "\".");
		
		//while (level >= 0) {
			byte[][] ciphertext;
			
			try {
				ciphertext = cipher.encryptWithLevel(level, message.getBytes());
			} catch (CipherToolsException e) {
				throw new DocumentToolsException("Internal error: cannot encrypt given message.");
			}
			
			encryptedParts.put(new Pair<Integer, Integer>(partID, level), ciphertext);
			
			//level--;
		//}
	}
	
	/**
	 * Put hashes for this document
	 * @param hash and encoded list of hash
	 */
	public void addHashes(String hash) {
		hashes = hash;
	}

	/**
	 * Send new document to the server and all the added parts.
	 * You can always add new parts to this document with addPart() and then
	 * recalling this method. 
	 * @return docID assigned by the server
	 */
	public Long submit() {
		System.out.println("Sending document and document parts.");
		
		if (docID < 0) {
			System.out.println("Document doesn't exist yet on the server. Creating a new one");
			if (createDocument() == null)
				return null;
		}
		
		Set<Pair<Integer, Integer>> parts = encryptedParts.keySet();
		
		// Send each part to the server
		Iterator<Pair<Integer, Integer>> i = parts.iterator();
		while (i.hasNext()) {
			Pair<Integer, Integer> key = i.next();
			
			int partPartID = key.getKey();
			int partLevel = key.getValue();
			
			byte[][] cipherArray = encryptedParts.get(key);
			
			byte[] iv = cipherArray[0];
			byte[] ciphertext = cipherArray[1];
			
			JSONObject json = new JSONObject();
			
			try {
				json.put("docID", docID);
				json.put("partID", partPartID);
				json.put("level", partLevel);
				json.put("ciphertext", CipherTools.encodeBase64(ciphertext));
				json.put("iv", CipherTools.encodeBase64(iv));
			} catch (JSONException e) {
				System.out.println("Internal error: JSONException while creating doc part JSON to send.");
				return null;
			}
			
			String response = connection.postJSONPart(json, docID, partPartID);
			
			if (response.length() == 0) {
				System.out.println("Cannot determine server status while sending partID " + partPartID + " for level " + partLevel + ".\nContinuing anyway.");
			}
		}
		
		// Remove sent parts 
		encryptedParts.clear();
		
		return docID;
	}
	
	/**
	 * @param message
	 * @return the signature of the given message
	 */
	public byte[] signMessage(byte[] message) {
		try {
			return cipher.signMessage(message);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| InvalidKeySpecException | SignatureException e) {
			System.out.println("Internal error: unable to sign message.");
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @param message
	 * @param sign
	 * @param username
	 * @return true if signature is valid or false if it is not or there was an error
	 */
	public boolean verifyMessage(byte[] message, byte[] sign, String username) {
		try {
			return cipher.verifyMessage(message, sign, username);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| InvalidKeySpecException | SignatureException | JSONException e) {
			System.out.println("Internal error: unable to verify signature.");
			return false;
		}
	}
	
	/* PRIVATE METHODS */

	private Long createDocument() {
		if (docID < 0) {
			
			// Generate a new document
			JSONObject json = new JSONObject();
			
			// Add title if set
			if (title != null && title.length() > 0) {
				try {
					json.put("docTitle", title);
				} catch (JSONException e) {
					System.out.println("Internal error: JSONException while creating document JSON to send.");
					return null;
				}
			}
			
			if (hashes != null && hashes.length() > 0) {
				try {
					json.put("hashes", hashes);
				} catch (JSONException e) {
					System.out.println("Internal error: JSONException while creating document JSON to send.");
					return null;
				}
			}
			
			// Send and read the response
			String response = connection.postJSONDocument(json);
			
			if (response.length() == 0) {
				System.out.println("Cannot determine new docID for given document.");
				return null;
			}
			
			try {
				JSONObject jsonResponse = new JSONObject(response);
				
				docID = jsonResponse.getLong("docID");
				
				System.out.println("New docID=" + docID);
			} catch (JSONException e) {
				System.out.println("Internal error: invalid JSON response from the server, cannot obtain docID");
				return null;
			}
			
		}
		
		return docID;
	}
	
	private JSONObject getDetails(long docID) throws JSONException {
		
		String jsonString = connection.getJSONDocDetails(docID);
		
		if (jsonString.length() == 0)
			return null;
		
		JSONObject json = new JSONObject(jsonString);
		return json;
	}

}
