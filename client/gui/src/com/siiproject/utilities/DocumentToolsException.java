package com.siiproject.utilities;

public class DocumentToolsException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DocumentToolsException(String m) {
		super(m);
	}
}
