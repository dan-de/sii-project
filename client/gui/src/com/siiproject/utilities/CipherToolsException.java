package com.siiproject.utilities;

public class CipherToolsException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CipherToolsException(String m) {
		super(m);
	}
}
