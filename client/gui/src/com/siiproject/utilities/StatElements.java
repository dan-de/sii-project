package com.siiproject.utilities;

import java.util.HashMap;

public class StatElements {
	public static final String MISS = "miss";
	public static final String MOD 	= "mod";
	public static final String OK 	= "ok";
	
	public static Long docId;
	public static Long docIdBkp;
	public static DocumentTools doc;
	public static DocumentTools docBkp;
	public static HashMap<Integer, Integer> idToLevel;
	public static HashMap<Integer, String[]> paragraph;
	public static Boolean verified;
	public static String toHash;
	public static Boolean creationComplete;
	public static Boolean redo = false;
	
	public static final String beginTag 	= "<X";
	public static final String endTag 		= "X>";
	public static final String endText 		= "<ETX>";
	
	public static final String fontPath 	= "./lib/FreeMono.ttf";
}
