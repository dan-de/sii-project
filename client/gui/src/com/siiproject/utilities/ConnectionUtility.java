package com.siiproject.utilities;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.json.JSONObject;

public class ConnectionUtility {
	public static final String propertiesFile = "settings.txt";
	public static final String serverKeystore = "local.keystore";
	private static final String serverKeystorePassword = "qwerty";
	
	public static final String defURL = "localhost";
	public static final String defProtocol = "https";
	public static final String defPort = "default";
	
	// For these URLs definitions check Django urls.py files in server folder
	public static final String RESTUserCredential = "/rest/credential/";
	public static final String RESTUserLevels = "/rest/levels/";
	public static final String RESTUserDocs = "/rest/docs/";
	public static final String RESTUserDSA = "/rest/users/"; // +username/
	
	private static ConnectionUtility instance = null;
	
	/*default*/ String username;
	/*default*/ String password;
	private Properties settings;
	private static String url = ""; // Required static by init_SSLSocket() method
	private String protocol;
	private String port;
	
	private boolean connected;
	
	private ConnectionUtility() {
		username = "";
		password = "";
		settings = null;
		url = "";
		protocol = "";
		port = "";
		connected = false;
	}
	
	public static ConnectionUtility getInstance() {
		if (instance == null)
			instance = new ConnectionUtility();
		return instance;
	}
	
	public static void destroyInstance() {
		System.out.println("Destroying connection with the server.");
		instance = null;
		url = "";
	}
	
	public void setUsername(String user) {
		username = user;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setPassword(String pass) {
		password = pass;
	}
	
	private HttpURLConnection getConnection(String urlPath) throws IOException {
		HttpURLConnection connection;
		
		URL urlObj = new URL(urlPath);
		
		if (protocol.equalsIgnoreCase("https")) {
			connection = (HttpsURLConnection)urlObj.openConnection();
		}
		else {
			connection = (HttpURLConnection)urlObj.openConnection();
		}
		
		// Open required channels
		connection.setDoInput(true);
		
		// Set credentials
		String credentials = username + ":" + password;
		connection.setRequestProperty("Authorization", "Basic " + CipherTools.encodeBase64(credentials.getBytes()));
		
		return connection;
	}
	
	/**
	 * Enstablish a connection with the server and check credentials
	 * @return true if user is authenticated, false otherwise or if errors
	 */
	public boolean connect() {
		
		if (connected)
			return true;
		
		readProperties();

		String urlPath = protocol + "://" + url + ":" + port + RESTUserCredential;
		
		System.out.println("Connecting to: " + urlPath);
		
		HttpURLConnection connection;
		try {
			
			if (protocol.equalsIgnoreCase("https"))
				init_SSLSocket();
			
			connection = getConnection(urlPath);
			
			// Using HEAD method: we just need to verify the credentials
			connection.setRequestMethod("HEAD");
			//connection.setRequestProperty("Accept", "application/json");
		} catch (IOException e1) {
			System.out.println("Internal Error.");
			return false;
		}
		
		try {
			
			int resp = connection.getResponseCode();
			
			if (resp < 200 || resp >= 300) {
				System.out.println("Invalid credential. Response code: " + resp + ": " + connection.getResponseMessage());
				return false;
			}
			
		} catch (IOException e) {
			System.out.println("Connection problems.");
			return false;
		}
		
		System.out.println("Connected.");
		connected = true;
		connection.disconnect();
		return true;
	}
	
	public boolean isConnected() {
		return connected;
	}
	
	/* PROTECTED METHODS */
	/* well... non actually protected :D */
	
	/**
	 * @return a JSON containing current logged user credentials
	 */
	String getJSONCredentials() {
		if (!isConnected())
			return "";
		
		String response;
		
		String urlPath = protocol + "://" + url + ":" + port + RESTUserCredential;
		
		System.out.println("Requested credentials to: " + urlPath);
		
		HttpURLConnection connection;
		try {
			connection = getConnection(urlPath);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
		} catch (IOException e1) {
			System.out.println("Internal Error.");
			return "";
		}
		
		try {
			
			int resp = connection.getResponseCode();
			
			if (resp < 200 || resp >= 300) {
				System.out.println("Unsuccessful response code from server: " + resp + ": " + connection.getResponseMessage());
				return "";
			}
			
			response = fetchJSON(connection);
			
		} catch (IOException e) {
			System.out.println("Connection problems.");
			return "";
		}

		connection.disconnect();
		return response;
	}
	
	/**
	 * @return a JSON containing a list of levels user can use to decrypt
	 */
	String getJSONUserLevelList() {
		if (!isConnected())
			return "";
		
		String response;
		
		String urlPath = protocol + "://" + url + ":" + port + RESTUserLevels;
		
		System.out.println("Requested user levels to: " + urlPath);
		
		HttpURLConnection connection;
		try {
			connection = getConnection(urlPath);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
		} catch (IOException e1) {
			System.out.println("Internal Error.");
			return "";
		}
		
		try {
			
			int resp = connection.getResponseCode();
			
			if (resp < 200 || resp >= 300) {
				System.out.println("Unsuccessful response code from server: " + resp + ": " + connection.getResponseMessage());
				return "";
			}
			
			response = fetchJSON(connection);
			
		} catch (IOException e) {
			System.out.println("Connection problems.");
			return "";
		}

		connection.disconnect();
		return response;
	}
	
	/**
	 * @param levNumber
	 * @return a JSON containing the AES level key encrypted with user key
	 */
	String getJSONUserLevel(int levNumber) {
		if (!isConnected())
			return "";
		
		String response;
		
		String urlPath = protocol + "://" + url + ":" + port + RESTUserLevels + levNumber + "/";
		
		System.out.println("Requested user levels to: " + urlPath);
		
		HttpURLConnection connection;
		try {
			connection = getConnection(urlPath);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
		} catch (IOException e1) {
			System.out.println("Internal Error.");
			return "";
		}
		
		try {
			
			int resp = connection.getResponseCode();
			
			if (resp < 200 || resp >= 300) {
				System.out.println("Unsuccessful response code from server: " + resp + ": " + connection.getResponseMessage());
				return "";
			}
			
			response = fetchJSON(connection);
			
		} catch (IOException e) {
			System.out.println("Connection problems.");
			return "";
		}

		connection.disconnect();
		return response;
	}
	
	/**
	 * @param username
	 * @return a JSON containing the public DSA key of the requested user
	 */
	String getJSONUserDSAPubKey(String username) {
		if (!isConnected())
			return "";
		
		String response;
		
		String urlPath = protocol + "://" + url + ":" + port + RESTUserDSA + username + "/";
		
		System.out.println("Requested user DSA key to: " + urlPath);
		
		HttpURLConnection connection;
		try {
			connection = getConnection(urlPath);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
		} catch (IOException e1) {
			System.out.println("Internal Error.");
			return "";
		}
		
		try {
			
			int resp = connection.getResponseCode();
			
			if (resp < 200 || resp >= 300) {
				System.out.println("Unsuccessful response code from server: " + resp + ": " + connection.getResponseMessage());
				return "";
			}
			
			response = fetchJSON(connection);
			
		} catch (IOException e) {
			System.out.println("Connection problems.");
			return "";
		}

		connection.disconnect();
		return response;
	}
	
	/**
	 * @param docID
	 * @return a JSON containing the details of the required document
	 */
	String getJSONDocDetails(long docID) {
		if (!isConnected())
			return "";
		
		String response;
		
		String urlPath = protocol + "://" + url + ":" + port + RESTUserDocs + docID + "/";
		
		System.out.println("Requested details for document " + docID);
		
		HttpURLConnection connection;
		try {
			connection = getConnection(urlPath);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
		} catch (IOException e1) {
			System.out.println("Internal Error.");
			return "";
		}
		
		try {
			
			int resp = connection.getResponseCode();
			
			if (resp < 200 || resp >= 300) {
				System.out.println("Unsuccessful response code from server: " + resp + ": " + connection.getResponseMessage());
				return "";
			}
			
			response = fetchJSON(connection);
			
		} catch (IOException e) {
			System.out.println("Connection problems.");
			return "";
		}

		connection.disconnect();
		return response;
	}
	
	/**
	 * @param docID
	 * @return a JSON containing a list of all parts associated with the given document
	 */
	String getJSONDocPartList(long docID) {
		if (!isConnected())
			return "";
		
		String response;
		
		String urlPath = protocol + "://" + url + ":" + port + RESTUserDocs + docID + "/?parts=true";
		
		System.out.println("Requested parts list for document: " + docID);
		
		HttpURLConnection connection;
		try {
			connection = getConnection(urlPath);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
		} catch (IOException e1) {
			System.out.println("Internal Error.");
			return "";
		}
		
		try {
			
			int resp = connection.getResponseCode();
			
			if (resp < 200 || resp >= 300) {
				System.out.println("Unsuccessful response code from server: " + resp + ": " + connection.getResponseMessage());
				return "";
			}
			
			response = fetchJSON(connection);
			
		} catch (IOException e) {
			System.out.println("Connection problems.");
			return "";
		}

		connection.disconnect();
		return response;
	}
	
	/**
	 * @param docID
	 * @param partID
	 * @return a JSON containing a list of level numbers that you can use to decrypt the given part for the given document
	 */
	String getJSONDocPartLevelList(long docID, int partID) {
		if (!isConnected())
			return "";
		
		String response;
		
		String urlPath = protocol + "://" + url + ":" + port + RESTUserDocs + docID + "/" + partID + "/";
		
		System.out.println("Requested level list for document " + docID + " and part " + partID);
		
		HttpURLConnection connection;
		try {
			connection = getConnection(urlPath);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
		} catch (IOException e1) {
			System.out.println("Internal Error.");
			return "";
		}
		
		try {
			
			int resp = connection.getResponseCode();
			
			if (resp < 200 || resp >= 300) {
				System.out.println("Unsuccessful response code from server: " + resp + ": " + connection.getResponseMessage());
				return "";
			}
			
			response = fetchJSON(connection);
			
		} catch (IOException e) {
			System.out.println("Connection problems.");
			return "";
		}

		connection.disconnect();
		return response;
	}
	
	/**
	 * @param docID
	 * @param partID
	 * @param level
	 * @return a JSON containing the ciphertext and the iv of the given part of the given document for the given level
	 */
	String getJSONDocPartCiphertext(long docID, int partID, int level) {
		if (!isConnected())
			return "";
		
		String response;
		
		String urlPath = protocol + "://" + url + ":" + port + RESTUserDocs + docID + "/" + partID + "/?level=" + level;
		
		System.out.println("Requested ciphertext for document " + docID + " and part " + partID);
		
		HttpURLConnection connection;
		try {
			connection = getConnection(urlPath);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
		} catch (IOException e1) {
			System.out.println("Internal Error.");
			return "";
		}
		
		try {
			
			int resp = connection.getResponseCode();
			
			if (resp < 200 || resp >= 300) {
				System.out.println("Unsuccessful response code from server: " + resp + ": " + connection.getResponseMessage());
				return "";
			}
			
			response = fetchJSON(connection);
			
		} catch (IOException e) {
			System.out.println("Connection problems.");
			return "";
		}

		connection.disconnect();
		return response;
	}
	
	/**
	 * @param json
	 * @return the JSON response of the server
	 */
	String postJSONDocument(JSONObject json) {
		if (!isConnected())
			return "";
		
		String response;
		
		String urlPath = protocol + "://" + url + ":" + port + RESTUserDocs;
		
		System.out.println("Creating document at: " + urlPath);
		
		HttpURLConnection connection;
		try {
			
			// Set connection
			
			connection = getConnection(urlPath);
			connection.setRequestMethod("POST");
			// Open required channels
			connection.setDoOutput(true);
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Content-Type", "application/json");
			
		} catch (IOException e1) {
			System.out.println("Internal Error: IOException while setting connection.");
			return "";
		}
		
		try {

			// Send JSON
			
			OutputStream os = connection.getOutputStream();
			DataOutputStream dos = new DataOutputStream(os);
			dos.write(json.toString().getBytes("UTF-8"));
			dos.write('\n');
			dos.flush();
			dos.close();
			os.close();
			
			// Check response code
			
			int resp = connection.getResponseCode();
			
			if (resp < 200 || resp >= 300) {
				System.out.println("Unsuccessful response code from server: " + resp + ": " + connection.getResponseMessage());
				return "";
			}
			
			//Fetch response
			
			response = fetchJSON(connection);
			
		} catch (IOException e) {
			System.out.println("Connection problems.");
			return "";
		}

		connection.disconnect();
		return response;
	}

	/**
	 * @param json
	 * @return the JSON response of the server
	 */
	String postJSONPart(JSONObject json, long docID, int partID) {
		if (!isConnected())
			return "";
		
		String response;
		
		String urlPath = protocol + "://" + url + ":" + port + RESTUserDocs + docID + "/" + partID + "/";
		
		System.out.println("Creating document part at: " + urlPath);
		
		System.out.println(json.toString());
		
		HttpURLConnection connection;
		try {
			
			// Set connection
			
			connection = getConnection(urlPath);
			connection.setRequestMethod("POST");
			// Open output channel
			connection.setDoOutput(true);
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Content-Type", "application/json");
			
		} catch (IOException e1) {
			System.out.println("Internal Error: IOException while setting connection.");
			return "";
		}
		
		try {

			// Send JSON
			
			OutputStream os = connection.getOutputStream();
			DataOutputStream dos = new DataOutputStream(os);
			dos.write(json.toString().getBytes("UTF-8"));
			dos.write('\n');
			dos.flush();
			dos.close();
			os.close();
			
			// Check response code
			
			int resp = connection.getResponseCode();
			
			if (resp < 200 || resp >= 300) {
				System.out.println("Unsuccessful response code from server: " + resp + ": " + connection.getResponseMessage());
				return "";
			}
			
			//Fetch response
			
			response = fetchJSON(connection);
			
		} catch (IOException e) {
			System.out.println("Connection problems.");
			return "";
		}

		connection.disconnect();
		return response;
	}
	
	/* PRIVATE METHODS */
	
	private void readProperties() {
		
		settings = new Properties();
		try {
			settings.load(new FileInputStream(propertiesFile));
		} catch (IOException e) {
			System.out.println("Unable to load settings file. Using default parameters.");
			
			settings.put("URL", defURL);
			settings.put("protocol", defProtocol);
			settings.put("port", defPort);
			
		}
		
		try {
			url = (String) settings.get("URL");
			
			if (!url.matches("^[a-zA-Z0-9.]+$")) {
				System.out.println("Invalid url, restoring default value.");
				url = defURL;
				settings.put("URL", defURL);
			}
			
		} catch (NullPointerException e) {
			url = defURL;
			settings.put("URL", url);
		}
		
		try {
			protocol = (String) settings.get("protocol");
			protocol = protocol.toLowerCase();
			
			if (!(protocol.equalsIgnoreCase("http") || protocol.equalsIgnoreCase("https"))) {
				System.out.println("Unknown protocol, restoring default value.");
				protocol = defProtocol;
				settings.put("protocol", defProtocol);
			}

		} catch (NullPointerException e) {
			protocol = defProtocol;
			settings.put("protocol", defProtocol);
		}
		
		try {
			port = (String) settings.get("port");
		} catch (NullPointerException e) {
			port = "default";
			settings.put("port", port);
		}
		
		if (port.equalsIgnoreCase("default")) {
			if (protocol.equalsIgnoreCase("http"))
				port = "80";
			else
				port = "443";
		}
		
		try {
			settings.store(new FileOutputStream(propertiesFile), "SII client properties");
		} catch (IOException e1) {
			System.out.println("Unable to store default settings. Going on anyway.");
		}
		
	}
	
	private static void init_SSLSocket() {
		
		/*
		 * Disable hostname checking for server url into the SSL certificate
		 */
		HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier(){

			@Override
			public boolean verify(String arg0, SSLSession arg1) {
				if (arg0.equals(url)) {
					return true;
	            }
				return false;
			}
			
		});
		
		/*
		 * Import keystore with server certificate
		 * or disable certificate validity checking
		 */
		try {
			
			System.out.print("Trying to import server certificate... ");
			
			KeyStore serverKeyStore = KeyStore.getInstance("JKS");
			serverKeyStore.load(new FileInputStream(serverKeystore), serverKeystorePassword.toCharArray());
			
			TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
			tmf.init(serverKeyStore);
			
			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, tmf.getTrustManagers(), new SecureRandom());
			
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
			
			System.out.println("OK!");
			
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | KeyManagementException | IOException e1) {
			
			System.out.println("FAILED!");
			
			System.out.println("Cannot import server certificate. Disabling certificate validations.");
			
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] { 
			    new X509TrustManager() {

					@Override
					public void checkClientTrusted(
							java.security.cert.X509Certificate[] chain,
							String authType) throws CertificateException {
					}

					@Override
					public void checkServerTrusted(
							java.security.cert.X509Certificate[] chain,
							String authType) throws CertificateException {
					}

					@Override
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}
			    } 
			};
			
			try {
				
				SSLContext sc = SSLContext.getInstance("SSL");
			    sc.init(null, trustAllCerts, new SecureRandom()); 
			    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			    
			} catch (NoSuchAlgorithmException | KeyManagementException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	private String fetchJSON(HttpURLConnection conn) throws IOException {
		
		InputStream content = conn.getInputStream();
		BufferedReader in = new BufferedReader (new InputStreamReader(content));
		String line;
		String json = "";
		while ((line = in.readLine()) != null) {
			json += line+"\n";
		}
		content.close();
		in.close();
		
		return json;
		
	}

}
