package com.siiproject.pdf;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.pdf.PRStream;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStream;
import com.itextpdf.text.pdf.parser.PdfImageObject;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;

public class PDFReader {
	
	public static final int FACTOR = 100;
	
	public File getImages(String path) throws IOException, BadElementException, ErrorCreatingDirectory{
		PdfReader reader;

	    File file = new File(path);
	    File dir = file.getParentFile();
	    File newTmpDir = new File(dir, "tmp");
	    if(!newTmpDir.mkdir() && !newTmpDir.exists()){
	    	throw new ErrorCreatingDirectory("mkdir is not possible!");
	    }
	    reader = new PdfReader(file.getAbsolutePath());
	    for (int i = 0; i < reader.getXrefSize(); i++) {
	        PdfObject pdfobj = reader.getPdfObject(i);
	        if (pdfobj == null || !pdfobj.isStream()) {
	            continue;
	        }
	        PdfStream stream = (PdfStream) pdfobj;
	        PdfObject pdfsubtype = stream.get(PdfName.SUBTYPE);
	        if (pdfsubtype != null && pdfsubtype.toString().equals(PdfName.IMAGE.toString())) {
	            PdfImageObject im = new PdfImageObject((PRStream) stream);
	            BufferedImage bi = im.getBufferedImage();
	            if (bi == null) continue;
                int width = (int)(bi.getWidth() * FACTOR);
                int height = (int)(bi.getHeight() * FACTOR);
                BufferedImage newbi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                AffineTransform at = AffineTransform.getScaleInstance(FACTOR, FACTOR);
                Graphics2D g = newbi.createGraphics();
                g.drawRenderedImage(newbi, at);
	            ByteArrayOutputStream imgBytes = new ByteArrayOutputStream();
	            ImageIO.write(bi, "JPG", imgBytes);
	            FileOutputStream out = new FileOutputStream(new File(newTmpDir, String.format("%1$05d", i) + ".png"));
	            imgBytes.writeTo(out);
	            out.flush();
	            out.close();
	        }
	    }
	    return newTmpDir;
	}
	
	public String readPDF(String path) throws IOException{
		PdfReader pr = new PdfReader(path);
		PdfReaderContentParser prcp = new PdfReaderContentParser(pr);
		String result = new String();
		TextExtractionStrategy strategy;
        for (int i = 1; i <= pr.getNumberOfPages(); i++) {
            strategy = prcp.processContent(i, new SimpleTextExtractionStrategy());
            if(!strategy.getResultantText().endsWith("\n"))
            	result = result.concat(strategy.getResultantText()+"\n");
            else
            	result = result.concat(strategy.getResultantText());
        }
        pr.close();
        return result;
	}
	
}
