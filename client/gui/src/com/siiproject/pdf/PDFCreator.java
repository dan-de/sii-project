package com.siiproject.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import com.itextpdf.text.pdf.qrcode.EncodeHintType;
import com.itextpdf.text.pdf.qrcode.ErrorCorrectionLevel;
import com.siiproject.reader.QRCodeStandard;
import com.siiproject.utilities.CipherTools;
import com.siiproject.utilities.StatElements;

public class PDFCreator {
	
	
	public void createPDF(String text, String path) throws DocumentException, IOException, NoSuchAlgorithmException, JSONException {
		HashMap<String, String> urls = new HashMap<String,String>();
		Document doc = new Document();
		
		String dltPath = path.concat("tmp");		
		PdfWriter pw = PdfWriter.getInstance(doc, new FileOutputStream(dltPath));
		doc.open();
		addContent(doc, text);
		doc.close();
		pw.close();
		pw = null;
		
		PdfReader pr = new PdfReader(dltPath);
		PdfReaderContentParser prcp = new PdfReaderContentParser(pr);
		StatElements.toHash = new String();
		TextExtractionStrategy strategy;
        for (int i = 1; i <= pr.getNumberOfPages(); i++) {
            strategy = prcp.processContent(i, new SimpleTextExtractionStrategy());
            if(!strategy.getResultantText().endsWith("\n"))
            	StatElements.toHash = StatElements.toHash.concat(strategy.getResultantText()+"\n");
            else
            	StatElements.toHash = StatElements.toHash.concat(strategy.getResultantText());
        }
        pr.close();
        
        String noSpace = StatElements.toHash.replace(" ", "");
        String[] noEndlArray = noSpace.split("\n");
        String noEndl = new String();
        for(int i=0;i<noEndlArray.length; i++){
        	if(!noEndlArray[i].equals(" ")){
        		noEndl = noEndl.concat(noEndlArray[i]);
        	}
        }
        QRCodeStandard sign = PDFCreator.genSignQR(noEndl);
		urls.put(QRCodeStandard.SIGNATUREID, sign.getContent());
		String hash = PDFCreator.hashCompute(StatElements.toHash);
		
		
		File f = new File(path);
		if(!StatElements.redo){
			StatElements.doc.setTitle(f.getName());
			StatElements.doc.addHashes(hash);
			StatElements.docId = StatElements.doc.submit();
		}
		QRCodeStandard table = PDFCreator.genTableQR();
		urls.put(QRCodeStandard.TABLE, table.getContent());
		
		doc = new Document();
		pw = PdfWriter.getInstance(doc, new FileOutputStream(path));
		doc.open();
		text = text.concat(StatElements.endText);
		addContent(doc, text);
		addQrCode(doc, urls);
		doc.close();
		File dlt = new File(dltPath);
		dlt.delete();
	}

	private void addContent(Document document, String text) throws DocumentException, IOException {
		//BaseFont ubuntu = BaseFont.createFont(StatElements.fontPath, BaseFont.WINANSI, BaseFont.EMBEDDED);
		Font font = new Font(Font.FontFamily.COURIER, 14);
		String[] par = text.split("\n");

		for(int i=0; i<par.length; i++){
			if(!par[i].equals("") && par[i]!=null)
				document.add(new Paragraph(par[i], font));
			else
				document.add(new Paragraph(" ", font));
		}
	}
	
	private void addQrCode(Document document, HashMap<String, String> urls) throws DocumentException{
		BarcodeQRCode qr;
		Map<EncodeHintType,Object> hint = new HashMap<EncodeHintType,Object>();
		hint.put(EncodeHintType.ERROR_CORRECTION,ErrorCorrectionLevel.L );
		Image img;
		PdfPTable ptable = new PdfPTable(2);
		ptable.setWidthPercentage(100);
		ptable.setSpacingBefore(10);
		PdfPCell pcell = new PdfPCell();
		Set<String> keys = urls.keySet();
		for(String i : keys){
			pcell = new PdfPCell();
			pcell.setPadding(20);
			pcell.setBorderColor(BaseColor.WHITE);
			qr = new BarcodeQRCode(i+":"+urls.get(i), 120, 120, hint);
			img = qr.getImage();
			pcell.addElement(img);
			ptable.addCell(pcell);
		}
		ptable.completeRow();
		ptable.setKeepTogether(true);
		document.add(ptable);
	}
	
	private static QRCodeStandard genTableQR() throws JSONException{
		JSONObject json = new JSONObject();
		json.put(QRCodeStandard.JSON_DOCID, StatElements.docId);
		Set<Integer> ids = StatElements.idToLevel.keySet();
		for(Integer key : ids){
			json.put(key.toString(), StatElements.idToLevel.get(key));
		}
		return new QRCodeStandard(QRCodeStandard.TABLE, json.toString());	
	}
	
	private static QRCodeStandard genSignQR(String text){
		byte[] toSign = text.getBytes();
		byte[] sign;
		sign = StatElements.doc.signMessage(toSign);
		String content = CipherTools.encodeBase64(sign);
		return new QRCodeStandard(QRCodeStandard.SIGNATUREID, content);
	}
	
	public static String hashCompute(String completeText) throws NoSuchAlgorithmException{
		String textNoSpace = completeText.replace(" ", "");
		String[] split = textNoSpace.split("\n");
		
		byte[] hash;
		String finale = new String();
		for(int i=0; i<split.length; i++){
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			if(split[i].equals("") || split[i].equals(" "))
				continue;
			else
				hash = sha.digest(split[i].getBytes());
			
			if(i!=0){
				finale = finale.concat(":"+CipherTools.encodeBase64(hash));
			}
			else{
				finale = finale.concat(CipherTools.encodeBase64(hash));
			}
		}
		return finale;
	}

}
