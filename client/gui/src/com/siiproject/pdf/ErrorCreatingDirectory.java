package com.siiproject.pdf;

public class ErrorCreatingDirectory extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ErrorCreatingDirectory(String s){
		super(s);
	}

}
