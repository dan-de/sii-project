package com.siiproject.reader;
import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Resizer {
	BufferedImage src = null;
	Graphics2D image = null;
	BufferedImage dst = null;
	int WIDTH;
	int HEIGHT;
	
	public Resizer(int width, int heigth) {
		WIDTH = width;
		HEIGHT = heigth;
	}
	
	public void loadImage(String path) throws IOException {
		BufferedImage img = ImageIO.read(new File(path));
		loadImage(img);
	}
	
	public void loadImage(BufferedImage img) {
		if (img!=null)
			src = img;
	}
	
	public BufferedImage resize() {
		if (src == null)
			return null;
		if (dst != null)
			return dst;
		
		int type = src.getType() == 0? BufferedImage.TYPE_INT_ARGB : src.getType();
		
		dst = new BufferedImage(WIDTH, HEIGHT, type);
		image = dst.createGraphics();
		image.drawImage(src, 0, 0, WIDTH, HEIGHT, null);
		image.dispose();
		
		image.setComposite(AlphaComposite.Src);
		 
		image.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		image.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		image.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		return dst;
	}
	
	public BufferedImage getImage() {
		return dst;
	}
	
	public void saveImage(String path) throws IOException {
		ImageIO.write(dst, "png", new File(path));
	}
}
