package com.siiproject.reader;

public class ErrorReadingQR extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ErrorReadingQR(String e){
		super(e);
	}

}
