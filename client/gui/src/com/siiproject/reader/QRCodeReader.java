package com.siiproject.reader;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

public class QRCodeReader {

	private static QRCodeStandard readFromQRCode(String path) throws FileNotFoundException, IOException, NotFoundException{
		BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
				new BufferedImageLuminanceSource(
						ImageIO.read(new FileInputStream(path)))));
		Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap,null);
		String result = qrCodeResult.getText();
		return QRCodeStandard.parseToQRCode(result);
	}

	public static ArrayList<QRCodeStandard> readMultipleQRCode(List<String> paths) throws FileNotFoundException, NotFoundException, IOException{
		ArrayList<QRCodeStandard> qrcodes = new ArrayList<QRCodeStandard>();
		File dir = (new File(paths.get(0))).getParentFile();
		for (int i=0; i<paths.size(); i++){
			File img = new File(paths.get(i));
			qrcodes.add(readFromQRCode(img.getAbsolutePath()));
			img.delete();
		}
		if(dir.list().length==0){
			dir.delete();
		}
		return qrcodes;
	}

	public static ArrayList<QRCodeStandard> readMultipleQRCode(File dir) throws FileNotFoundException, NotFoundException, IOException, ErrorReadingQR{
		ArrayList<QRCodeStandard> qrcodes = new ArrayList<QRCodeStandard>();
		File[] images = dir.listFiles();
		for (int i=0; i<images.length; i++){
			if(!images[i].isFile() || !images[i].getName().endsWith(".png")) continue;
			QRCodeStandard qr = readFromQRCode(images[i].getAbsolutePath());
			if(qr == null){
				throw new ErrorReadingQR("QRCode unreadable");
			}
			qrcodes.add(qr);
			images[i].delete();
		}
		if(dir.list().length==0){
			dir.delete();
		}
		return qrcodes;
	}


	/** This function take an image and divide it in rows*column parts
	 * @param offseth in the point y height where start to construct
	 * @param offsetw is the point x of width where start to construct
	 * @return an array of buffered image that contains the chunks
	 * */
	public static BufferedImage[]  IterativeSplitImage(BufferedImage image,int rows, int column, int offseth, int offsetw,int chunkHeight) throws IOException{

		String ris[] = null;
		int cols = column;
		int chunks = rows * cols;
		ris=new String [chunks];
		int chunkWidth = image.getWidth() / cols; // determines the chunk width and height


		int count = 0;
		BufferedImage imgs[] = new BufferedImage[chunks]; //Image array to hold image chunks
		for (int x = 0; x < rows; x++) {
			for (int y = 0; y < cols; y++) {
				//Initialize the image array with image chunks
				imgs[count] = new BufferedImage(chunkWidth, chunkHeight, image.getType());

				// draws the image chunk
				Graphics2D gr = imgs[count++].createGraphics();
				gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, (chunkWidth * y)+offsetw, (chunkHeight * x)+offseth, chunkWidth * y + chunkWidth+offsetw, chunkHeight * x + chunkHeight+offseth, null);

				gr.dispose();
			}
		}			
		return imgs;
	}

	/**
	 * read a qr code in a buffered path
	 * */
	private static QRCodeStandard readFromQRCode(BufferedImage path) throws FileNotFoundException, IOException, NotFoundException{
		BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
				new BufferedImageLuminanceSource(path)));
		Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap,null);
		String result = qrCodeResult.getText();
		return QRCodeStandard.parseToQRCode(result);
	}
 
	/** 
	 * find qr codes in a image
	 * @throws NotFoundException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * */

	public static ArrayList<String> parseImage(String path) throws ErrorReadingQR, FileNotFoundException, IOException{

		ArrayList <String> result=new ArrayList<String>();

		//Resizer r=new Resizer(300,300);

		//qui avviamo una lettura ciclica del documento incrementando offseth fino a che non riusciamo a leggere
		int offseth=0;	// scendere 10 pixel per volta dovrebbe permettere di saltare riga per riga
		int offsetw=10;	// impostare valore di un approssimativo margine del foglio

		//righe e colonne con cui vogliamo dividere
		int rows=1; 
		int column=2; //numero di qr per riga

		FileInputStream fis;

		fis = new FileInputStream(new File(path));
		BufferedImage image = ImageIO.read(fis); //reading the image file
		Resizer r=new Resizer(800,800);
		r.loadImage(image);
		r.resize();
		int width=image.getWidth();
		int heigth=image.getHeight();
		int chunkHeight = heigth/4;
		//i qr ho visto che sono 130x130 circa quindi è inutile scansionare parti piu piccole di tale grandezza
		// inoltre non posso provare a leggere meno del valore impostato a chunkHeight
		while(offseth<=(heigth-chunkHeight)){ //i qr ho visto che sono 130x130 circa quindi è inutile scansionare parti piu piccole di tale grandezza

			BufferedImage temp[]=IterativeSplitImage(r.getImage(), rows, column, offseth, offsetw,chunkHeight);
			ArrayList<QRCodeStandard> qrs = new ArrayList<QRCodeStandard>();



			for(BufferedImage tempimg : temp){// provo a leggere le 4 immagini, 
				// se tutte hanno testo, aggiungo alla lista result e continuo alla riga successiva perchè potrebbero essercene altri
				// se nessuno ha testo e lista vuota, continuo la ricerca. Altrimenti il resto del foglio è vuoto
				// se solo alcuni hanno testo, inserisco in lista e chiudo entrambi i cicli
				QRCodeStandard qr = null;

				try {
					qr = readFromQRCode(tempimg);
				} catch (NotFoundException e) {
				}

				if(qr!=null){
					qrs.add(qr);
				}

			}
			if(!qrs.isEmpty()){
				for(int index = 0; index<qrs.size(); index++){
					String temps=qrs.get(index).getQRText();
					if(!result.contains(temps)){			
						result.add(temps);
						System.out.println(temps);
					}

				}
			}
			offseth+=10;
		}



		return result;

	}


	public static BufferedImage scale(BufferedImage sbi, int imageType, int dWidth, int dHeight, double fWidth, double fHeight) {
		BufferedImage dbi = null;
		if(sbi != null) {
			dbi = new BufferedImage(dWidth, dHeight, imageType);
			Graphics2D g = dbi.createGraphics();
			AffineTransform at = AffineTransform.getScaleInstance(fWidth, fHeight);
			g.drawRenderedImage(sbi, at);
		}
		return dbi;
	}

}
