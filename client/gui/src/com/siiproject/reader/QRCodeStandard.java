package com.siiproject.reader;

public class QRCodeStandard {
	String id;
	String content;
	public static final String SIGNATUREID = "sign";
	public static final String HASH = "hash";
	public static final String TABLE = "table";
	
	public static final String JSON_DOCID = "docid";
	
	public QRCodeStandard() {
	}
	
	public QRCodeStandard(String id, String content){
		this.id = id;
		this.content = content;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
	public void setContent(String content){
		this.content=content;
	}
	
	public String getId(){
		return id;
	}
	
	public String getContent(){
		return content;
	}
	
	public static QRCodeStandard parseToQRCode(String text){
		String[] fields = text.split(":", 2);
		if(fields.length!=2){
			return null;
		}
		return new QRCodeStandard(fields[0], fields[1]);
	}
	
	public String getQRText(){
		return id.concat(":"+content);
	}

}
