package com.siiproject.reader;

public class NotVerifiedDocument extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NotVerifiedDocument(String e){
		super(e);
	}
	
}
