package com.siiproject.reader;

public class BadFormatDocument extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BadFormatDocument(String e){
		super(e);
	}
}
