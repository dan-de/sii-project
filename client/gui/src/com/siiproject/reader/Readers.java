package com.siiproject.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.crypto.NoSuchPaddingException;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.itextpdf.text.BadElementException;
import com.siiproject.ocr.OcrClass;
import com.siiproject.pdf.ErrorCreatingDirectory;
import com.siiproject.pdf.PDFCreator;
import com.siiproject.pdf.PDFReader;
import com.siiproject.utilities.CipherTools;
import com.siiproject.utilities.CipherToolsException;
import com.siiproject.utilities.ConnectionUtility;
import com.siiproject.utilities.DocumentTools;
import com.siiproject.utilities.DocumentToolsException;
import com.siiproject.utilities.StatElements;

public class Readers {
	
	public static final String CONTENT = "content";
	public static HashMap<String, String> results;
	
	public static String readOnlyEncryptImages(List<String> images, String lang) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException, DocumentToolsException, CipherToolsException, JSONException, FileNotFoundException, ErrorReadingQR, IOException{
		StatElements.doc = new DocumentTools(ConnectionUtility.getInstance(), CipherTools.getInstance(ConnectionUtility.getInstance()));
		String result = new String();
		OcrClass oc = new OcrClass();
		
		if(lang.equals(OcrClass.NONE)){
			for(int i=0; i<images.size(); i++){
				result = result.concat(oc.doOCRNoLang(images.get(i)));
			}
		}
		else{
			for(int i=0; i<images.size(); i++){
				result = result.concat(oc.doOCR(images.get(i), lang));
			}
		}
		
		if(result.contains(StatElements.endText)){
			int index = result.lastIndexOf(StatElements.endText);
			result = result.substring(0, index);
		}
		
		results = new HashMap<String, String>();
		results.put(CONTENT, result);

		ArrayList<String> qrcodes = QRCodeReader.parseImage(images.get(images.size()-1));
		if(qrcodes.isEmpty() || qrcodes.size()!=2){
			throw new ErrorReadingQR("Error reading QR codes from images");
		}
		
		for(int i = 0; i<qrcodes.size(); i++){
			QRCodeStandard newqr = QRCodeStandard.parseToQRCode(qrcodes.get(i));
			results.put(newqr.getId(), newqr.getContent());
		}
		
		return results.get(CONTENT);
	}
	
	public static void decryptImages(String newText) throws BadFormatDocument, JSONException, NoSuchAlgorithmException{
		results.put(CONTENT, newText);
		String tableJson = results.get(QRCodeStandard.TABLE);
		if(tableJson==null){
			throw new BadFormatDocument("Document not supported");
		}
		
		
		String toSignNoSpace =  results.get(CONTENT).replace(" ", "");
		String[] toSignArray = toSignNoSpace.split("\n");
		String toSign = new String();
		for(int i=0;i<toSignArray.length; i++){
        	if(!toSignArray[i].equals(" ")){
        		toSign = toSign.concat(toSignArray[i]);
        	}
        }
		StatElements.docId = ((Number)(new JSONObject(tableJson)).get(QRCodeStandard.JSON_DOCID)).longValue();
		if(!StatElements.doc.verifyMessage(toSign.getBytes(), CipherTools.decodeBase64(results.get(QRCodeStandard.SIGNATUREID)), StatElements.doc.getCreator(StatElements.docId))){
			StatElements.verified = false;
			checkHashes(results);
		}
		else{
			StatElements.verified = true;
			popolateParagraph(results);
		}
		
		manageCryptedText(tableJson);
	}
	
	public static void readEncryptedImages(List<String> images, String lang) throws DocumentToolsException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException, CipherToolsException, JSONException, ErrorReadingQR, BadFormatDocument, NotFoundException, FileNotFoundException, IOException{
		StatElements.doc = new DocumentTools(ConnectionUtility.getInstance(), CipherTools.getInstance(ConnectionUtility.getInstance()));
		String result = new String();
		OcrClass oc = new OcrClass();
		
		if(lang.equals(OcrClass.NONE)){
			for(int i=0; i<images.size(); i++){
				result = result.concat(oc.doOCRNoLang(images.get(i)));
			}
		}
		else{
			for(int i=0; i<images.size(); i++){
				result = result.concat(oc.doOCR(images.get(i), lang));
			}
		}
		
		if(result.contains(StatElements.endText)){
			int index = result.lastIndexOf(StatElements.endText);
			result = result.substring(0, index);
		}
		
		results = new HashMap<String, String>();
		results.put(CONTENT, result);

		ArrayList<String> qrcodes = QRCodeReader.parseImage(images.get(images.size()-1));
		if(qrcodes.isEmpty() || qrcodes.size()!=2){
			throw new ErrorReadingQR("Error reading QR codes from images");
		}
		
		for(int i = 0; i<qrcodes.size(); i++){
			QRCodeStandard newqr = QRCodeStandard.parseToQRCode(qrcodes.get(i));
			results.put(newqr.getId(), newqr.getContent());
		}
		
		String tableJson = results.get(QRCodeStandard.TABLE);
		if(tableJson==null){
			throw new BadFormatDocument("Document not supported");
		}
		
		String toSign = results.get(CONTENT).replace("\n", "");
		StatElements.docId = ((Number)(new JSONObject(tableJson)).get(QRCodeStandard.JSON_DOCID)).longValue();
		if(!StatElements.doc.verifyMessage(toSign.getBytes(), CipherTools.decodeBase64(results.get(QRCodeStandard.SIGNATUREID)), StatElements.doc.getCreator(StatElements.docId))){
			StatElements.verified = false;
			checkHashes(results);
		}
		else{
			StatElements.verified = true;
			popolateParagraph(results);
		}
		
		manageCryptedText(tableJson);
	}
	
	public static String readNormalImages(List<String> images, String lang){
		String result = new String();
		OcrClass oc = new OcrClass();
		
		if(lang.equals(OcrClass.NONE)){
			for(int i=0; i<images.size(); i++){
				result = result.concat(oc.doOCRNoLang(images.get(i)));
			}
		}
		else{
			for(int i=0; i<images.size(); i++){
				result = result.concat(oc.doOCR(images.get(i), lang));
			}
		}		
		return result;
	}
	
	public static void readEncryptedPdf(String path) throws IOException, NotFoundException, BadElementException, JSONException, DocumentToolsException, ChecksumException, FormatException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException, CipherToolsException, BadFormatDocument, NotVerifiedDocument, ErrorReadingQR, ErrorCreatingDirectory{
		StatElements.creationComplete = false;
		
		StatElements.doc = new DocumentTools(ConnectionUtility.getInstance(), CipherTools.getInstance(ConnectionUtility.getInstance()));
		String resultText = "";
		PDFReader pr = new PDFReader();
		resultText = pr.readPDF(path);
		if(resultText.contains(StatElements.endText)){
			resultText = resultText.replace(StatElements.endText, "");
		}
		HashMap<String, String> results = new HashMap<String, String>();
		results.put(CONTENT, resultText);
		File tmpDir = pr.getImages(path);
		ArrayList<QRCodeStandard> qrcodes = QRCodeReader.readMultipleQRCode(tmpDir);
		for(int i=0; i<qrcodes.size(); i++){
			results.put(qrcodes.get(i).getId(),qrcodes.get(i).getContent());
		}
		String tableJson = results.get(QRCodeStandard.TABLE);
		if(tableJson==null){
			throw new BadFormatDocument("Document not supported");
		}
		
		String toSignNoSpace =  results.get(CONTENT).replace(" ", "");
		String[] toSignArray = toSignNoSpace.split("\n");
		String toSign = new String();
		for(int i=0;i<toSignArray.length; i++){
        	if(!toSignArray[i].equals(" ")){
        		toSign = toSign.concat(toSignArray[i]);
        	}
        }
		StatElements.docId = ((Number)(new JSONObject(tableJson)).get(QRCodeStandard.JSON_DOCID)).longValue();
		boolean verified = StatElements.doc.verifyMessage(toSign.getBytes(), CipherTools.decodeBase64(results.get(QRCodeStandard.SIGNATUREID)), StatElements.doc.getCreator(StatElements.docId));
		if(!verified){
			StatElements.verified = false;
			checkHashes(results);
		}
		else{
			StatElements.verified = true;
			popolateParagraph(results);
		}
		manageCryptedText(tableJson);
		
		StatElements.creationComplete = true;
	}
	
	private static void popolateParagraph(HashMap<String, String> results) {
		String text = results.get(CONTENT);
		StatElements.paragraph = new HashMap<Integer, String[]>();
		
		String[] textSplit = text.split("\n");
		
		for(int i=0; i<textSplit.length; i++){
			String[] node = new String[2];
			node[0] = StatElements.OK;
			node[1] = textSplit[i];
			StatElements.paragraph.put(i, node);
		}
	}

	private static void checkHashes(HashMap<String, String> results) throws NoSuchAlgorithmException {
		String text = results.get(CONTENT);
		ArrayList<String> paragraph = new ArrayList<String>();
		ArrayList<String> status = new ArrayList<String>();
		StatElements.paragraph = new HashMap<Integer, String[]>();
		String servHash = StatElements.doc.getHashes(StatElements.docId);
		String textHash = PDFCreator.hashCompute(text);
		
		String[] textSplit = text.split("\n");
		
		String[] servHashes = servHash.split(":");
		String[] textHashes = textHash.split(":");
		int set = 0;
		for (int i=0, h=0; i<textHashes.length && h<textSplit.length; i++, h++){
			if(textSplit[h].equals("") || textSplit[h].equals(" ")){
				status.add(StatElements.OK);
				paragraph.add(textSplit[h]);
				i--;
			}
			else{
				Boolean ctrl = false;
				for(int j=set ; j<servHashes.length && ctrl==false; j++){
					if(textHashes[i].equals(servHashes[j])){
						ctrl=true;
						for(int k=set; k<j-1 && k<servHashes.length; k++){
							status.add(StatElements.MISS);
							paragraph.add(null);
						}
						set = j;
						status.add(StatElements.OK);
						paragraph.add(textSplit[h]);
					}
					else{
						System.out.println("Paragrafo non verificato");
						/* do nothing */
					}
				}
				if(!ctrl){
					set++;
					status.add(StatElements.MOD);
					paragraph.add(textSplit[h]);
				}
			}
		}
		for(int i=0; i<paragraph.size(); i++){
			String[] node = new String[2];
			node[0] = status.get(i);
			node[1] = paragraph.get(i);
			StatElements.paragraph.put(i, node);
		}
	}

	public static String readNormalPdf(String path) throws IOException{
		String resultText = "";
		PDFReader pr = new PDFReader();
		resultText = pr.readPDF(path);
		return resultText;
	}
	 
	private static void manageCryptedText(String tableJson){
		StatElements.idToLevel = new HashMap<Integer,Integer>();
		String decrypt;
		String partial = new String();
		Boolean stop = false;
		int startIndex = 0, endIndex = 0, lastIndex = 0;
		int id = 0;
		
		Set<Integer> par = StatElements.paragraph.keySet();
		for(Integer i : par){
			String crypt = StatElements.paragraph.get(i)[1];
			decrypt = new String();
			lastIndex = 0;
			stop = false;
			if(crypt!=null){
				while(!stop){
					startIndex = crypt.indexOf(StatElements.beginTag, lastIndex); // Point at '<'%
					if(startIndex<0){
						stop = true;
					}
					else{
						decrypt = decrypt.concat(crypt.substring(lastIndex, startIndex));
						startIndex++; // Now points to <'%'
						endIndex = crypt.indexOf(StatElements.endTag, startIndex+1); //From <%' '
						if (endIndex > 0) {
							partial = crypt.substring(startIndex+1, endIndex);
							endIndex = endIndex + 2;
							try {
								id = Integer.parseInt(partial); //TODO questa funzione può fallire
								StatElements.idToLevel.put(id, ((Number)(new JSONObject(tableJson)).get(partial)).intValue());
								String message = StatElements.doc.getPart(StatElements.docId, id);
								if(message!=null){
									decrypt = decrypt.concat(message);
								} else {
									decrypt = decrypt.concat(StatElements.beginTag+partial+StatElements.endTag);
								}
							} catch (NumberFormatException | JSONException e) {
								System.out.println("Error reading a partID in the document:"+StatElements.beginTag+partial+StatElements.endTag+". Probably misunderstanding of the OCR library?");
								decrypt = decrypt.concat(StatElements.beginTag + partial + StatElements.endTag);
							}
						} else {
							decrypt = decrypt.concat(StatElements.beginTag);
							endIndex = startIndex + 1;
						}
						lastIndex = endIndex;
					}
				}
				decrypt = decrypt.concat(crypt.substring(lastIndex));
				StatElements.paragraph.get(i)[1]=decrypt;
			}
		}
	}

}
