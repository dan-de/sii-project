/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.siiproject.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author stefano
 */

public class LoginView extends Application {

	@Override
	public void start(Stage stage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("/com/siiproject/gui/fxml_login.fxml"));

		MyScenes.loginscene = new Scene(root, 600, 400);
		stage.setTitle("SII Project");
		stage.setScene(MyScenes.loginscene);
		stage.setMaxHeight(400);
		stage.setMinHeight(400);
		stage.setMaxWidth(600);
		stage.setMinWidth(600);
		MyScenes.loginstage=stage;
		stage.show();
	}
	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
