package com.siiproject.gui;



import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ErrorDialog {
	
	public static Stage dialogStage ;
	
	public static void showErrorDialog(String error){
		dialogStage = new Stage();
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initStyle(StageStyle.UTILITY);
		dialogStage.setTitle("Error");
		Text t=new Text(error);
		t.setFont(new Font(16));
		dialogStage.setScene(new Scene(VBoxBuilder.create().
				children(t).
				alignment(Pos.CENTER).padding(new Insets(5)).build()));
		dialogStage.sizeToScene();
		dialogStage.setMaxHeight(dialogStage.getHeight());
		dialogStage.setMaxWidth(dialogStage.getWidth());
		dialogStage.setMinHeight(dialogStage.getHeight());
		dialogStage.setMinWidth(dialogStage.getWidth());
		dialogStage.show();
		dialogStage.toFront();
	}
	
	public static void close(){
		dialogStage.hide();
	};

}

