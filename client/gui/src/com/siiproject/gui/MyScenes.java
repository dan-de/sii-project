package com.siiproject.gui;

import javafx.scene.Scene;
import javafx.stage.Stage;

public class MyScenes {
	public static Scene scene;
	public static Scene mainscene;	
	public static Scene cipherscene;
	public static Scene textscene;
	public static Scene textsceneblocked;
	public static Scene loginscene;
	public static Scene imagescene;
	public static Scene infoscene;
	public static Scene levelscene;
	public static Scene filescene;
	public static Scene readingscene;

	
	public static Stage mainstage;
	public static Stage loginstage;
	public static Stage cipherstage;
	public static Stage textstage;
	public static Stage textstageblocked;
	public static Stage imagestage;
	public static Stage infostage;
	public static Stage levelstage;
	public static Stage filestage;
	public static Stage readingstage;
}
