/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.siiproject.gui;

import java.io.IOException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import com.siiproject.utilities.CipherTools;
import com.siiproject.utilities.ConnectionUtility;


public class LoginController extends Application {
	@FXML public Text actiontarget;
	@FXML public TextField name;
	@FXML public PasswordField password;
	@FXML public Button login;

	@FXML public void handleSubmitButtonAction(ActionEvent event) {
		login();
	}

	protected void login(){
		ConnectionUtility conn;
		try {
			// Get credentials
			String user = name.getText();
			String pass = password.getText();

			// Get the ConnectionUtility instance
			conn = ConnectionUtility.getInstance();
			conn.setUsername(user);
			conn.setPassword(pass);

			// Try to connect
			if (!conn.connect()) {
				actiontarget.setText("Connection problems.");
				return;
			}
			MyScenes.loginscene.getWindow().hide();
			Parent root = FXMLLoader.load(getClass().getResource("/com/siiproject/gui/fxml_maingui.fxml"));
			MyScenes.mainscene= new Scene(root, 200, 200);
			Stage stage = new Stage();
			stage.setMinHeight(200);
			stage.setMinWidth(200);
			stage.setTitle("SII Project");
			stage.setScene(MyScenes.mainscene);
			MyScenes.mainstage=stage;
			actiontarget.setText("");
			stage.show();

			//hide this current window (if this is want you want)

		} catch (IOException e) {
			actiontarget.setText("Connection problems.");
			return;
		}
	}

	protected static void DestroyConnection(){
		ConnectionUtility.destroyInstance();
		CipherTools.destroyInstance();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

	}

}
