package com.siiproject.gui;

import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;

import javax.crypto.NoSuchPaddingException;

import org.json.JSONException;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.DocumentException;
import com.siiproject.reader.BadFormatDocument;
import com.siiproject.reader.ErrorReadingQR;
import com.siiproject.reader.NotVerifiedDocument;
import com.siiproject.reader.Readers;
import com.siiproject.utilities.CipherTools;
import com.siiproject.utilities.CipherToolsException;
import com.siiproject.utilities.ConnectionUtility;
import com.siiproject.utilities.DocumentToolsException;
import com.siiproject.utilities.StatElements;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

import com.siiproject.pdf.*;

public class FileCreationController {

	public String filepath;
	public String image_filepath,pdf_filepath;

	public String readTextPdf = new String();
	public static String highlated;

	
	
	@FXML public static TextArea textarea;
	
	@FXML public Button next;
	@FXML public Button back;
	@FXML public Button encript;
	@FXML public Button cancel;
	@FXML public Button image;
	@FXML public Button pdf;
	@FXML public Label label;
	@FXML public Button generate;
	@FXML public Button destination_button;
	@FXML public TextField destination_text;
	
	@FXML public void handleCancel(ActionEvent event){

		MyScenes.filestage.hide();
		MyScenes.mainstage.show();
	}

	@FXML public void handleImageOcr(){
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/com/siiproject/gui/images_choose.fxml"));
		} catch (IOException e) {
			ErrorDialog.showErrorDialog("FXML Error: "+e.getMessage());
			return;
		}

		MyScenes.imagescene= new Scene(root);
		MyScenes.imagestage = new Stage();
		MyScenes.imagestage.setTitle("Image chooser");
		MyScenes.imagestage.setScene(MyScenes.imagescene);
		ImagesChooseController.setModality("create");
		MyScenes.imagestage.show();
		MyScenes.mainstage.hide();
	}
	
	@FXML public void handleFolder(ActionEvent event) {
		 FileChooser directoryChooser = new FileChooser();
		 directoryChooser.getExtensionFilters().addAll(
					new ExtensionFilter("PDF Files", "*.pdf"),
					new ExtensionFilter("All Files", "*.*")
					);
         File selectedDirectory = 
                 directoryChooser.showSaveDialog(null);
          
         if(selectedDirectory == null){
             destination_text = null;
         }else{
        	 destination_text.setText(selectedDirectory.getAbsolutePath());
         }
	}
	
	@FXML public void handlePdfRead(){
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(
				new ExtensionFilter("PDF Files", "*.pdf"),
				new ExtensionFilter("All Files", "*.*")
				);

		//Show open file dialog
		File file = fileChooser.showOpenDialog(null);

		String read = new String();
		try {
			if(file==null) return;
			pdf_filepath=file.getAbsolutePath();
			if(pdf_filepath==null) return;
			read = Readers.readNormalPdf(pdf_filepath);
			
		} catch (IllegalArgumentException | IOException ex) {
			ErrorDialog.showErrorDialog("Error reading PDF: "+ex.getMessage());
			return;
			
		}
		readTextPdf = read;
		textarea.appendText(readTextPdf);
	}

	@FXML public void handleEncript() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException, CipherToolsException, JSONException{
		String temp=textarea.getSelectedText();
		if(temp.isEmpty()){
			
			return;
		}
		
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/com/siiproject/gui/level_choice.fxml"));
		} catch (IOException e) {
			ErrorDialog.showErrorDialog("FXML Error: "+e.getMessage());
			return;
		}

		MyScenes.levelscene= new Scene(root);
		
		MyScenes.levelstage = new Stage();
		MyScenes.levelstage.setTitle("Select security level");
		MyScenes.levelstage.setScene(MyScenes.levelscene);
		LevelChoiceController.selection.appendText(temp);
		
		
		LevelChoiceController.set(textarea.getSelection().getStart(),textarea.getSelection().getEnd(), CipherTools.getInstance(ConnectionUtility.getInstance()).getLevels());
		
		MyScenes.levelstage.show();
		
		MyScenes.filestage.toBack();
		
	}	
	
	@FXML public void handleNext(){
		
		if(textarea.getText().isEmpty()){
			ErrorDialog.showErrorDialog("No text written");
			return ;
		}
			
		if(StatElements.idToLevel==null){
			StatElements.idToLevel = new HashMap<Integer, Integer>();
		}
		next.setVisible(false);
		textarea.setEditable(false);
		cancel.setVisible(false);
		pdf.setVisible(false);
		image.setVisible(false);
		label.setVisible(false);
		
		back.setVisible(true);	
		encript.setVisible(true);
		generate.setVisible(true);
		destination_button.setVisible(true);
		destination_text.setVisible(true);
		
	}	
	
	@FXML public void handleBack(){
		back.setVisible(false);	
		encript.setVisible(false);
		generate.setVisible(false);
		destination_button.setVisible(false);
		destination_text.setVisible(false);
		LevelChoiceController.reset_id();
		
		next.setVisible(true);
		textarea.setEditable(true);
		cancel.setVisible(true);
		pdf.setVisible(true);
		image.setVisible(true);
		label.setVisible(true);
	}
	
	@FXML public void handleGeneration(){
		PDFCreator pdf_creator= new PDFCreator();
		
		if (destination_text == null || destination_text.getText().isEmpty()){
			ErrorDialog.showErrorDialog("Missing destination folder");
			return;
		}
		
		String path = destination_text.getText();
		if(!path.endsWith(".pdf"))
			path = path.concat(".pdf");
		
		try {
			pdf_creator.createPDF(textarea.getText(), path);
		} catch (DocumentException | IOException | NoSuchAlgorithmException | JSONException e) {
			ErrorDialog.showErrorDialog("Error generating the new document: "+e.getMessage());
			return;
		}
		
		StatElements.docIdBkp = StatElements.docId; 
		StatElements.docBkp = StatElements.doc;
		
		try {
			Readers.readEncryptedPdf(path);
		} catch (InvalidKeyException | NotFoundException | BadElementException
				| ChecksumException | FormatException
				| NoSuchAlgorithmException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | InvalidKeySpecException
				| IOException | JSONException | DocumentToolsException
				| CipherToolsException | BadFormatDocument
				| NotVerifiedDocument | ErrorReadingQR | ErrorCreatingDirectory e) {
			;
		}
		
		if(StatElements.creationComplete){
			StatElements.idToLevel = null;
			StatElements.redo=false;
			textarea.clear();
			destination_text.clear();
			LevelChoiceController.reset_id();
			MyScenes.filestage.hide();
			MyScenes.mainstage.show();
		}
		else{
			ErrorDialog.showErrorDialog("Document creation failed! Please try again");
			StatElements.redo = true;
			StatElements.doc = StatElements.docBkp;
			StatElements.docId = StatElements.docIdBkp;
		}
	}	

	public static void setData(String text){
		textarea.clear();
		textarea.appendText(text);
	}

	

}
