package com.siiproject.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleGroup;


public class CodificationController {

	public final static ToggleGroup Sicurezza=new ToggleGroup();
	@FXML public static RadioButton r1;
	@FXML public static RadioButton r2;
	@FXML public static RadioButton r3;
	@FXML public static TextArea textarea;
	
	@FXML public void handleAnnul(ActionEvent event){
		
		MyScenes.cipherstage.hide();
		MyScenes.mainstage.show();
	}
	
	@FXML public void handleOk(){
		
	}

	public static void setData(String text){
		r1.setToggleGroup(Sicurezza);
		r1.setSelected(true);
		r2.setToggleGroup(Sicurezza);
		r3.setToggleGroup(Sicurezza);
		
		
		textarea.appendText(text);
		
	}
	
}
