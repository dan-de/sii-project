package com.siiproject.gui;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.NoSuchPaddingException;

import org.json.JSONException;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.itextpdf.text.BadElementException;
import com.siiproject.pdf.ErrorCreatingDirectory;
import com.siiproject.reader.BadFormatDocument;
import com.siiproject.reader.ErrorReadingQR;
import com.siiproject.reader.NotVerifiedDocument;
import com.siiproject.reader.Readers;
import com.siiproject.utilities.CipherTools;
import com.siiproject.utilities.CipherToolsException;
import com.siiproject.utilities.ConnectionUtility;
import com.siiproject.utilities.DocumentTools;
import com.siiproject.utilities.DocumentToolsException;
import com.siiproject.utilities.StatElements;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;

public class MainController {

	/****
	 * controller dell'interfaccia principale fxml_maingui.fxml
	 * 
	 */

	String filepath;
	@FXML public BorderPane pane;
	@FXML protected ImageView imageview1;
	//@FXML public Button ocr_button;

	String pdf_filepath;
	String readTextPdf = new String();

	
	//@FXML public static ChoiceBox<String> choicebox;

	BufferedImage bufferedImage;
	MouseEvent mouseEvent;
	static Rectangle rect;
	static SimpleDoubleProperty rectinitX ;
	static SimpleDoubleProperty rectinitY ;
	static SimpleDoubleProperty rectX = new SimpleDoubleProperty();
	static SimpleDoubleProperty rectY = new SimpleDoubleProperty();

	BufferedImage image;
	int dx,dy,dwidth,dheigth,dx2,dy2;

	/**
	 * handle button load images
	 * */
	@FXML public void handleLoadImageAction(ActionEvent event) {


		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/com/siiproject/gui/images_choose.fxml"));
		} catch (IOException e) {
			System.err.println("Error in handleLoadImageAction: "+e.getMessage());
		}

		MyScenes.imagescene= new Scene(root);
		MyScenes.imagestage = new Stage();
		MyScenes.imagestage.setTitle("Image chooser");
		MyScenes.imagestage.setScene(MyScenes.imagescene);
		ImagesChooseController.setModality("read");
		MyScenes.imagestage.show();
		MyScenes.mainstage.hide();

	}

	@FXML public void handleLoadPDFAction(ActionEvent event) {

		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(
				new ExtensionFilter("PDF Files", "*.pdf"),
				new ExtensionFilter("All Files", "*.*")
				);

		//Show open file dialog
		File file = fileChooser.showOpenDialog(null);
		String results = null;
		
		try {
			if(file==null) return;
			pdf_filepath=file.getAbsolutePath();
			Readers.readEncryptedPdf(pdf_filepath);
			
		} catch (IllegalArgumentException | IOException | NotFoundException | BadElementException | JSONException | DocumentToolsException | ChecksumException | FormatException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeySpecException | CipherToolsException ex) {
			ErrorDialog.showErrorDialog("Error reading encrypted PDF: "+ex.getMessage());
			return;
			
		} catch (BadFormatDocument | NotVerifiedDocument e) {
			ErrorDialog.showErrorDialog("Bad Document: "+e.getMessage());
			return;
		} catch (ErrorReadingQR e) {
			ErrorDialog.showErrorDialog("Problem with QRCodes: "+e.getMessage());
			return;
		} catch (ErrorCreatingDirectory e) {
			ErrorDialog.showErrorDialog("Error creating temporary directory: "+e.getMessage());
			return;
		}
		
		readTextPdf = results;
		
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/com/siiproject/gui/info_file.fxml"));
		} catch (IOException e) {
			ErrorDialog.showErrorDialog("FXML error: "+e.getMessage());
			return;
		}
		MyScenes.infoscene = new Scene(root, 600, 470);
		MyScenes.infostage = new Stage();
		MyScenes.infostage.setTitle("Reader");
		MyScenes.infostage.setMinHeight(470);
		MyScenes.infostage.setMinWidth(600);
		MyScenes.infostage.setScene(MyScenes.infoscene);
		InfoFileController.setData();
		MyScenes.infostage.show();
	}


	@FXML public void handleLogout(ActionEvent event){
		MyScenes.mainstage.hide();
		LoginController.DestroyConnection();
		MyScenes.loginstage.show();
		
	}


	
	@FXML public void handleCreateFile(ActionEvent event){
		showTextStage();
	}

	@FXML public void handleTextSelection(ActionEvent event) {
		showTextStage();
	}

	private  void showTextStage(){
		try {
			StatElements.doc = new DocumentTools(ConnectionUtility.getInstance(), CipherTools.getInstance(ConnectionUtility.getInstance()));
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | InvalidAlgorithmParameterException
				| InvalidKeySpecException | DocumentToolsException
				| CipherToolsException | JSONException e1) {
			e1.printStackTrace();
		}
		Parent root = null;
		try {												
			root = FXMLLoader.load(getClass().getResource("/com/siiproject/gui/file_creation.fxml"));
		} catch (IOException e) {			
			e.printStackTrace();
		}
		MyScenes.filescene = new Scene(root, 600, 400);
		MyScenes.filestage = new Stage();
		MyScenes.filestage.setTitle("Create file");
		MyScenes.filestage.setScene(MyScenes.filescene);
		MyScenes.mainstage.hide();
		MyScenes.filestage.show();
		
	}
}
