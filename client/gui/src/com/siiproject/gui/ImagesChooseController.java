package com.siiproject.gui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.crypto.NoSuchPaddingException;

import org.json.JSONException;

import com.siiproject.ocr.OcrClass;
import com.siiproject.reader.ErrorReadingQR;
import com.siiproject.reader.Readers;
import com.siiproject.utilities.CipherToolsException;
import com.siiproject.utilities.DocumentToolsException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;

public class ImagesChooseController {
	
	@FXML
	static ChoiceBox<String> lang;

	private static String modality;
	static ObservableList<String> observableList=FXCollections.observableArrayList();
	public static String language;
	
	int image_index=0; //indice per le immagini
	BufferedImage bufferedImage[];
	String tempfilepath;

	@FXML public ListView<String> listview;

	@FXML public void handleUp (ActionEvent event) {

		if(listview.getSelectionModel().getSelectedItems().isEmpty()){
			System.out.println("nessun oggetto selezionato");
			return;
		}


		int position=listview.getSelectionModel().getSelectedIndex();
		String temp=listview.getSelectionModel().getSelectedItem();

		if (position==0) {
			listview.getSelectionModel().select(position);
			listview.getFocusModel().focus(position);
			return; // first element
		}

		observableList.remove(listview.getSelectionModel().getSelectedItem());
		observableList.add(position-1, temp);
		listview.setItems(observableList);
		listview.getSelectionModel().select(position-1);
		listview.getFocusModel().focus(position-1);

	}

	@FXML public void handleDown(ActionEvent event) {

		if(listview.getSelectionModel().getSelectedItems().isEmpty()){
			System.out.println("nessun oggetto selezionato");
			return;
		}

		int position=listview.getSelectionModel().getSelectedIndex();
		String temp=listview.getSelectionModel().getSelectedItem();

		if (position==observableList.size()-1){
			listview.getSelectionModel().select(position);
			listview.getFocusModel().focus(position);
			return; //last element
		}

		observableList.remove(listview.getSelectionModel().getSelectedItem());
		observableList.add(position+1, temp);
		listview.setItems(observableList);
		listview.getSelectionModel().select(position+1);
		listview.getFocusModel().focus(position+1);

	}

	@FXML public void handleAdd (ActionEvent event) {
		if(observableList.isEmpty())
			observableList = FXCollections.observableArrayList();

		FileChooser fileChooser = new FileChooser();


		fileChooser.getExtensionFilters().addAll(
				new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"),
				new ExtensionFilter("All Files", "*.*")
				);

		//Show open file dialog
		List<File> files = fileChooser.showOpenMultipleDialog(null);
		try {
			if(files!=null){
				for(File file: files ){
					tempfilepath=file.getAbsolutePath();
					observableList.add(file.getAbsolutePath().toString());
					listview.setItems(observableList);
				}
			}
			

		} catch (IllegalArgumentException ex) {
			ErrorDialog.showErrorDialog("Error adding image: "+ex);
		}
	}

	@FXML public void handleRemove(ActionEvent event) {
		if(observableList.isEmpty()) return;
		observableList.remove(listview.getSelectionModel().getSelectedItem());
		listview.setItems(observableList);

	}
	
	@FXML public void handleOk(ActionEvent event) {
		language = lang.getValue();
		
		if(language == null){
			language=OcrClass.NONE;
		}
		
		try{
			if(observableList.isEmpty()) return;
		}catch (NullPointerException e){
			ErrorDialog.showErrorDialog("Images not selected");
			return;
		}
		
		String readText = null;
		if(modality.equals("read")){
			try {
				
			readText = Readers.readOnlyEncryptImages(observableList, language);
				
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| NoSuchPaddingException
					| InvalidAlgorithmParameterException
					| InvalidKeySpecException | DocumentToolsException
					| CipherToolsException | JSONException | ErrorReadingQR | IOException e) {
				ErrorDialog.showErrorDialog("Error decrypting images: "+e);
				return;
			}
			showText(readText);
			//ErrorDialog.close();
		}
		else if(modality.equals("create")){ 
			ErrorDialog.showErrorDialog("ATTENDERE");
			readText = Readers.readNormalImages(observableList, language);
			ErrorDialog.close();
			setCreate(readText);
		}

	}

	@FXML public void handleEsc(ActionEvent event) {
		if(observableList!=null )observableList.clear();
		if(MyScenes.readingstage!=null) MyScenes.readingstage.hide();
		if(MyScenes.infostage!=null) MyScenes.infostage.hide();
		
		
		MyScenes.imagescene.getWindow().hide();
		
		if(modality.equals("read")) MyScenes.mainstage.show();
		else if(modality.equals("create")) MyScenes.filestage.show();
	}

	private void showText(String s){
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/com/siiproject/gui/read_stage.fxml"));
		} catch (IOException e) {
			ErrorDialog.showErrorDialog("FXML error:"+e.getMessage());
		}
		MyScenes.readingscene= new Scene(root, 600, 470);
		MyScenes.readingstage = new Stage();
		MyScenes.readingstage.setTitle("Correct any read errors");
		MyScenes.readingstage.setMinHeight(470);
		MyScenes.readingstage.setMinWidth(600);
		MyScenes.readingstage.setScene(MyScenes.readingscene);
		ReadingController.setReadingArea(s);
		MyScenes.readingstage.show();
	}
	
	private void setCreate(String s) {
		MyScenes.imagestage.hide();
		FileCreationController.setData(s);
		MyScenes.filestage.show();
	}

	protected  static void setModality(String s){
		modality=s;
		lang.getItems().addAll(FXCollections.observableArrayList(
			    OcrClass.ITA, OcrClass.ENG, OcrClass.NONE));
	}
	
	protected static String getModality(){
		return modality;
	}

}
