package com.siiproject.gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

import com.siiproject.utilities.StatElements;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class InfoFileController {
	

	@FXML public static WebView webview;
	@FXML public static VBox vbox;
	@FXML public static Label creator;
	@FXML public static Label title;
	@FXML public static Label creation_date;
	@FXML public static Label verified;
	@FXML public static TextArea ids;
	
	
	
	public static void setData(){

		String textColor = colorText();
		WebEngine webEngine = webview.getEngine();
		webEngine.loadContent(textColor);		
		creator.setText("Creator: "+ StatElements.doc.getCreator(StatElements.docId));
		title.setText("Title: "+StatElements.doc.getTitle(StatElements.docId));
		creation_date.setText("Created: "+StatElements.doc.getCreationDate(StatElements.docId));
		if(StatElements.verified)
			verified.setText("Verified: OK!");
		else
			verified.setText("Verified: Not verified");
		
		if(StatElements.idToLevel != null){
			Set<Integer> id = StatElements.idToLevel.keySet();
			ArrayList<Integer> idsordered = new ArrayList<Integer>(id);
			Collections.sort(idsordered);
			ArrayList<String> toPrint = new ArrayList<String>();
			for(Integer i : idsordered){
				toPrint.add("Id "+Integer.toString(i)+": Level "+Integer.toString(StatElements.idToLevel.get(i))+"\n");
			}
			populate_area(toPrint);
			StatElements.idToLevel=null;
		}
		
	}

	private static String colorText() {
		String result = new String();
		Set<Integer> keys = StatElements.paragraph.keySet();
		ArrayList<Integer> keysordered = new ArrayList<Integer>(keys);
		Collections.sort(keysordered);
		result = "<html>";
		for (Integer i : keysordered){
			String par = StatElements.paragraph.get(i)[1];
			if(par!=null){
				par = par.replaceAll("<", "&lt;");
				par = par.replaceAll(">", "&gt;");
			}
			if(StatElements.paragraph.get(i)[0].equals(StatElements.MOD)){
				result = result.concat("<font color=\"red\">"+par+"</font><br />");
			}
			else if(StatElements.paragraph.get(i)[0].equals(StatElements.MISS)){
				result = result.concat("<font color=\"red\">Missing line</font><br />");
			}
			else if(StatElements.paragraph.get(i)[0].equals(StatElements.OK)){
				result = result.concat(par+"<br />");
			}
		}
		result = result.concat("</html>");
		return result;
	}
	public static void populate_area(ArrayList<String> id){
		for(String s: id){
			ids.appendText(s);
		}
	}
}
