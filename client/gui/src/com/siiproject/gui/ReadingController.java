package com.siiproject.gui;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.json.JSONException;

import com.siiproject.reader.BadFormatDocument;
import com.siiproject.reader.Readers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class ReadingController {
	
	
	@FXML static TextArea area;

	@FXML public void handleNext(ActionEvent e){
		try {	
			Readers.decryptImages(area.getText());
		} catch (NoSuchAlgorithmException
				| JSONException | BadFormatDocument ex) {
			ErrorDialog.showErrorDialog("Error decrypting images: "+ex);
			return;
		}
		showInfo();
	}
	
	@FXML public void handleBack(ActionEvent e){
		MyScenes.readingstage.hide();
		MyScenes.imagestage.show();
	}
	
	public static void setReadingArea(String s){
		populate_area(s);
	}
	
	private static void populate_area(String s){
		area.appendText(s);
	}
	
	private void showInfo(){
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/com/siiproject/gui/info_file.fxml"));
		} catch (IOException e) {
			ErrorDialog.showErrorDialog("FXML error:"+e.getMessage());
		}
		MyScenes.infoscene= new Scene(root, 600, 470);
		MyScenes.infostage = new Stage();
		MyScenes.infostage.setTitle("Reader");
		MyScenes.infostage.setMinHeight(470);
		MyScenes.infostage.setMinWidth(600);
		MyScenes.infostage.setScene(MyScenes.infoscene);
		InfoFileController.setData();
		MyScenes.readingstage.hide();
		MyScenes.infostage.show();
	}

}
