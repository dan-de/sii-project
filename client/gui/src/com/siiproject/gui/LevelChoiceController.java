package com.siiproject.gui;

import java.util.Set;

import com.siiproject.utilities.DocumentToolsException;
import com.siiproject.utilities.StatElements;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;

public class LevelChoiceController {
	static int start;
	static int end;
	
	@FXML public static TextArea selection;
	@FXML public static CheckBox lev1;
	@FXML public static CheckBox lev2;
	@FXML public static CheckBox lev3;
	@FXML public static ChoiceBox<String> levels;
	
	static int id=1;
	
	@FXML public void handleOk(ActionEvent event) {
		String temp=FileCreationController.textarea.getText();
		String crypt = temp.substring(start, end);
		
		if (start!=0) temp=temp.substring(0,start)+StatElements.beginTag+Integer.toString(id)+StatElements.endTag+temp.substring(end,temp.length());
		else temp=StatElements.beginTag+Integer.toString(id)+StatElements.endTag+temp.substring(end,temp.length());
		String level = levels.getValue();
		if(level!=null){
			level = level.replaceAll("lev", "");
			level = level.trim();
			StatElements.idToLevel.put(id, Integer.parseInt(level));
			try {
				StatElements.doc.addPart(id, Integer.parseInt(level), crypt);
			} catch (DocumentToolsException e) {
				ErrorDialog.showErrorDialog("Error encrypting text: "+e);
			}
			id++;
			FileCreationController.setData(temp);
		
			MyScenes.levelstage.hide();
			MyScenes.filestage.toFront();
		}
		else{
			ErrorDialog.showErrorDialog("No level selected");
			return;
		}
	}
	
	@FXML public void handleBack(ActionEvent event) {
		MyScenes.levelstage.hide();
		MyScenes.filestage.toFront();
	}
	
	public static void set(int s,int e, Set<Integer> set){
		
		start=s; end=e;
		levels.setValue("first");
		ObservableList<String> observableList=FXCollections.observableArrayList();;
		
		for(int i: set){
			observableList.add("lev "+Integer.toString(i));
		}
		levels.setItems(observableList);
	}
	
	public static void reset_id(){
		id=1;
	}
}
