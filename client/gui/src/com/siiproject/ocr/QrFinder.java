package com.siiproject.ocr;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.GlobalHistogramBinarizer;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.multi.GenericMultipleBarcodeReader;
import com.google.zxing.multi.MultipleBarcodeReader;

public class QrFinder {
	
	
	private static final Map<DecodeHintType,Object> HINTS;
	private static final Map<DecodeHintType,Object> HINTS_PURE;

	static {
		HINTS = new EnumMap<DecodeHintType,Object>(DecodeHintType.class);
		HINTS.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
		HINTS.put(DecodeHintType.POSSIBLE_FORMATS, EnumSet.allOf(BarcodeFormat.class));
		HINTS_PURE = new EnumMap<DecodeHintType,Object>(HINTS);
		HINTS_PURE.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
	}
	
	public static String []  splitImage(String path) throws IOException{
		String ris[] = null;
		  FileInputStream fis = new FileInputStream(new File(path));
	        BufferedImage image = ImageIO.read(fis); //reading the image file

	        int rows = 2; //You should decide the values for rows and cols variables
	        int cols = 2;
	        int chunks = rows * cols;
	        ris=new String [chunks];
	        int chunkWidth = image.getWidth() / cols; // determines the chunk width and height
	        int chunkHeight = image.getHeight() / rows;
	        int count = 0;
	        BufferedImage imgs[] = new BufferedImage[chunks]; //Image array to hold image chunks
	        for (int x = 0; x < rows; x++) {
	            for (int y = 0; y < cols; y++) {
	                //Initialize the image array with image chunks
	                imgs[count] = new BufferedImage(chunkWidth, chunkHeight, image.getType());

	                // draws the image chunk
	                Graphics2D gr = imgs[count++].createGraphics();
	                gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, chunkWidth * y, chunkHeight * x, chunkWidth * y + chunkWidth, chunkHeight * x + chunkHeight, null);
	                gr.dispose();
	            }
	        }
	        System.out.println("Splitting done");

	        //writing mini images into image files
	        for (int i = 0; i < imgs.length; i++) {
	        	String s= new String ("img" + i + ".jpg");
	            ImageIO.write(imgs[i], "jpg", new File(s));
	            ris[i]=s;
	        }
	        System.out.println("Mini images created");
		return ris;
	}
	
	
	public static void readqr (String filePath) throws NotFoundException, ChecksumException, FormatException{
		BufferedImage image = null;
		try {
			image = ImageIO.read(new File(filePath));
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		Reader reader = new MultiFormatReader();
		LuminanceSource source = new BufferedImageLuminanceSource(image);
		BinaryBitmap bitmap = new BinaryBitmap(new GlobalHistogramBinarizer(source));
		Collection<Result> results = new ArrayList<Result>(1);
		// Look for multiple barcodes
		MultipleBarcodeReader multiReader = new GenericMultipleBarcodeReader(reader);
		Result[] theResults = multiReader.decodeMultiple(bitmap, HINTS);
		if (theResults != null) {
			System.out.println("letto ");
			results.addAll(Arrays.asList(theResults));
		}
		

		if (results.isEmpty()) {
			// Look for pure barcode
			Result theResult = reader.decode(bitmap, HINTS_PURE);
			if (theResult != null) {
				System.out.println("letto "+theResult);
				results.add(theResult);
			}
			
		}

		if (results.isEmpty()) {
			// Look for normal barcode in photo
			Result theResult = reader.decode(bitmap, HINTS);
			if (theResult != null) {
				System.out.println("letto "+theResult);
				results.add(theResult);
			}
		}

		if (results.isEmpty()) {
			// Try again with other binarizer
			BinaryBitmap hybridBitmap = new BinaryBitmap(new HybridBinarizer(source));
			Result theResult = reader.decode(hybridBitmap, HINTS);
			if (theResult != null) {
				System.out.println("letto "+theResult);
				results.add(theResult);
			}
			
		}
		System.out.println("Decodificato: ");
		for (Result r : results) {
			System.out.println(r);
		}
	}
	
	
	
}
