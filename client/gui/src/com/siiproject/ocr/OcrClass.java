package com.siiproject.ocr;


public class OcrClass {
	public static String ITA = "ita";
	public static String ENG = "eng";
	public static String NONE = "none";

	public native String doOCR(String path, String lang);	
	public native String doOCRNoLang(String path);
	
	static{
		System.loadLibrary("jniocr");
	}
}
