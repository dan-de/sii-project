#!/bin/bash

# Make sure only root can run our script
#if [[ $EUID -ne 0 ]]; then
   #echo "This script must be run as root" 1>&2
   #exit 1
#fi

sudo -v

#Installation of tesseract and leptonica libraries
sudo apt-get install libpng12-dev libtiff5-dev build-essential automake libtool

wget http://www.leptonica.com/source/leptonica-1.71.tar.gz
wget https://tesseract-ocr.googlecode.com/files/tesseract-ocr-3.02.02.tar.gz
wget https://tesseract-ocr.googlecode.com/files/tesseract-ocr-3.02.eng.tar.gz
wget https://tesseract-ocr.googlecode.com/files/tesseract-ocr-3.02.ita.tar.gz
tar -zxvf leptonica-1.71.tar.gz
tar -zxvf tesseract-ocr-3.02.02.tar.gz
tar -zxvf tesseract-ocr-3.02.eng.tar.gz
tar -zxvf tesseract-ocr-3.02.ita.tar.gz
rm *.tar.gz

cd leptonica-1.71
echo "Leptonica autobuild..."
./autobuild
echo "Leptonica configure..."
./configure
echo "Leptonica make..."
make
echo "Leptonica make install..."
sudo make install
echo "Leptonica ldconfig..."
sudo ldconfig

cd ..
cd tesseract-ocr
echo "Tesseract autogen.sh..."
./autogen.sh
echo "Tesseract configure..."
./configure
echo "Tesseract make..."
make
echo "Tesseract make install..."
sudo make install
echo "Tesseract ldconfig..."
sudo ldconfig

sudo cp -r tessdata /usr/local/share/
cd ..
make
