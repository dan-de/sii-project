#!/bin/bash

sudo -v

cd leptonica-1.71
echo "Leptonica make uninstall..."
sudo make uninstall
echo "Leptonica ldconfig..."
sudo ldconfig

cd ..
cd tesseract-ocr
echo "Tesseract make uninstall..."
sudo make uninstall
echo "Tesseract ldconfig..."
sudo ldconfig

cd ..

sudo rm -r leptonica-1.71
sudo rm -r tesseract-ocr

make clean
