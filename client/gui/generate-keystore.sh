#!/bin/bash

SERVERKEY="server.pem"
KEYSTOREPASSWORD="qwerty"

if [ ! -e $SERVERKEY ] ; then
	echo -e "Unable to find server certificate.\nPlease, add $SERVERKEY file under this folder." >&2
	exit 1
fi

echo "Generating server public keystore with pub key"
keytool -import -noprompt -keystore local.keystore -file $SERVERKEY -storepass $KEYSTOREPASSWORD

echo -e "Remember to set correct password for the keystore in java code!\n(Look for ConnectionUtility.java)"

exit 0
