# Client for SII Project

## Prerequisites

In order to make this program ready to work, you need to install some dependecies first.
Since the Graphical User Interface is written in Java FX and given that the current stable
version of OpenJDK does not include these libraries you need to install the Oracle JDK 7.
From Ubuntu you can install it with:

		~# apt-get install oracle-java7-installer

Other required libraries will be installed during the installation process.



## Installing dependencies

*gui* folder is an Eclipse project you can import directly into the IDE.
The project automatically uses the Oracle JDK if it is installed in the system.

This program make use of a JNI connector in order to use an OCR library initially
written for C++. You need to install some dependencies for this module and to
download additional packages. The process is automatic, you just need to run
**install_libraries.sh** script under *gui/lib/* folder:

		~$ cd gui/lib/
		~$ ./install_libraries.sh

This should be sufficient to download additional libraries, install them in the
system and compile the dynamic C++ library used by JNI connector.
If you want to recompile this library use the **Makefile** under the *gui/lib/*
folder.



## Running program

Before executing the program a couple of steps are required.

### JNI library path
First you need to specify to the Java Virtual Machine where the C++ library is.
To do that open Eclipse and go to *Run* menu and then *Run Configurations...*.
Under Java Application select the Java class related to the entry point of the
project. It should be *Login View*.
Move to *Arguments* tab and add under *VM Arguments:* the following line:

		-Djava.library.path=lib/

### Java keystore
This step, instead, is not mandatory, but highly recommended.
Probably (unless you used a signed SSL certificate for HTTPS server) your
Apache server is using a self signed certificate. By default Java checks
the authenticity of SSL certificates using his known Certification Authorities.
In order to let you connect to the Apache server with self signed certificates
this program checks if you prepared a keystore with the server certificate,
otherwise it disables validity checks and goes on with execution.
You shoud prepare this keystore for Java.
First get the Apache *.pem* file used for SSL connection. It shoud be under
*/etc/ssl/certs* folder. Check the exact location under the Apache *Virtual Server*
related to the HTTPS connections.
Put the *.pem* file under the *gui/* folder and run the **generate-keystore.sh**
script:

		~$ ./generate-keystore.sh

Now you have a file *local.keystore* under *gui/* that represent the keystore with
the server certificate. From now on all connections will be safe.

### Possible connection problems
When creating your self signed certificate for Apache (see Server README) you should
put the exact domain your client will connect to (eg. "localhost" if client and server
are under the same computer). If you use a different domain as *Fully Qualified Domain
Name* you won't log in. To avoid this modify the *Hostname* static String in
*ConnectionUtility* class in order to match the hostname you are referring to.



## Uninstall

You can remove all libraries installed during the install process simply running
the **uninstall_libraries.sh** script:

		~$ cd gui/lib/
		~$ ./uninstall_libraries.sh

To remove the project, simply delete it from Eclipse.
