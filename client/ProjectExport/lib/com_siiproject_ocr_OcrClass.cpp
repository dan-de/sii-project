#include <jni.h>
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>
#include <iostream>
#include "com_siiproject_ocr_OcrClass.h" 
#include <locale.h>
 
using namespace std; 
 
JNIEXPORT jstring JNICALL Java_com_siiproject_ocr_OcrClass_doOCR(JNIEnv *env, jobject obj, jstring path, jstring lang){
	const char *nativeString = env->GetStringUTFChars(path, 0);
	const char *langString = env->GetStringUTFChars(lang, 0);
	const char* none = "none";
	
	char *outText;

	setlocale(LC_NUMERIC, "C");
    tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();
    
	// Initialize tesseract-ocr with lang, without specifying tessdata path
	if (api->Init(NULL, langString)) {
		std::cerr << "Could not initialize tesseract.\n";
		exit(1);
	}
	
    // Open input image with leptonica library
    Pix *image = pixRead(nativeString);
    api->SetImage(image);
    // Get OCR result
    outText = api->GetUTF8Text();
    //printf("OCR output:\n%s", outText);

    // Destroy used object and release memory
    api->End();
    //delete [] outText;
    pixDestroy(&image);
    env->ReleaseStringUTFChars(path, nativeString);
	env->ReleaseStringUTFChars(lang, langString);

    return env->NewStringUTF(outText);
}

JNIEXPORT jstring JNICALL Java_com_siiproject_ocr_OcrClass_doOCRNoLang(JNIEnv *env, jobject obj, jstring path){
	const char *nativeString = env->GetStringUTFChars(path, 0);
	const char* none = "none";
	
	char *outText;

	setlocale(LC_NUMERIC, "C");
    tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();
    
    // Initialize tesseract-ocr with English, specifying tessdata path
	if (api->Init("./tessdata/", "eng")) {
		std::cerr << "Could not initialize tesseract.\n";
		exit(1);
	}
	
    // Open input image with leptonica library
    Pix *image = pixRead(nativeString);
    api->SetImage(image);
    // Get OCR result
    outText = api->GetUTF8Text();
    //printf("OCR output:\n%s", outText);

    // Destroy used object and release memory
    api->End();
    //delete [] outText;
    pixDestroy(&image);
    env->ReleaseStringUTFChars(path, nativeString);
	
    return env->NewStringUTF(outText);
}
