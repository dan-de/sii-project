# Installation guide

- Run **INSTALL** script to install the software dependecies
- Run the program via *run.sh*
- Note: configure the *settings.txt* created after the first run

# Uninstallation guide

- Simply run the **UNINSTALL** script
